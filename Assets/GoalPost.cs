﻿using UnityEngine;
using System.Collections;

public class GoalPost : MonoBehaviour {

	[SerializeField]
	AudioClip HitPost;

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Ball") {
			AudioSource.PlayClipAtPoint(HitPost, transform.position);
		}
	}
}
