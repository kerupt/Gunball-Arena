﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class CharacterStatsPanel : MonoBehaviour {

	public GameObject Char1;
	public GameObject Char2;
	public GameObject Char3;
	public GameObject Char4;

	public Sprite Char1Stats;
	public Sprite Char2Stats;
	public Sprite Char3Stats;
	public Sprite Char4Stats;

	
	// Update is called once per frame
	void Update () {
		if (EventSystem.current.currentSelectedGameObject == Char1){
			gameObject.GetComponent<Image>().sprite = Char1Stats;
		} else if (EventSystem.current.currentSelectedGameObject == Char2){
			gameObject.GetComponent<Image>().sprite = Char2Stats;
		} else if (EventSystem.current.currentSelectedGameObject == Char3){
			gameObject.GetComponent<Image>().sprite = Char3Stats;
		} else if (EventSystem.current.currentSelectedGameObject == Char4){
			gameObject.GetComponent<Image>().sprite = Char4Stats;
		} 
		
	}
}
