﻿using UnityEngine;
using System.Collections;

public class LastPlayerHitBy : MonoBehaviour {

	public int mLastPlayerHitBy = 0;
	int mStageHitCount;
	int mStageHitCountMax = 2;

	/// <summary>
	/// Returns 0 if both player haven't hit in a while
	/// </summary>
	/// <returns>The last player the ball was hit by</returns>
	public int GetLastPlayerHitBy()
	{
		return mLastPlayerHitBy;
	}

	public void PlayerHit(int playerNumber)
	{
		SetHitBy (playerNumber);
	}

	public void OtherHit()
	{
		if (mStageHitCount-- != 0)
			return;
		SetHitBy (0);
	}

	private void SetHitBy(int hitBy)
	{
		mStageHitCount = mStageHitCountMax;
		mLastPlayerHitBy = hitBy;
	}
}
