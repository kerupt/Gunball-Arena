﻿using UnityEngine;
using System.Collections;

public class FireWorks : MonoBehaviour {
	public float delayedStart = 3.5f;
	private float timer = 0;
	private bool playing = false;
	AudioSource fireworks;
	
	void Start() {
		AudioSource[] allMyAudioSources = GetComponents<AudioSource>();
		fireworks = allMyAudioSources[0];
	}
	
	// Update is called once per frame
	void Update () {
		if (!playing){
			timer += Time.deltaTime;
			if (timer >= delayedStart){
				fireworks.Play();
				playing = true;
			}
		}

	}
}
