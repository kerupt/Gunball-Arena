﻿using UnityEngine;
using System.Collections;

public class GoalSizeHandler : MonoBehaviour {

	float mInitialTopPostY;
	float mInitialBottomPostY;
	float mInitialGoalAreaYSize;
	float mYSizeModifer;

	[SerializeField]
	GameObject mPostTop;
	[SerializeField]
	GameObject mPostBottom;
	[SerializeField]
	GameObject mGoalArea;

	float mInitialYModifier;

	void Start () {
		mInitialTopPostY = mPostTop.transform.localPosition.y;
		mInitialBottomPostY = mPostBottom.transform.localPosition.y;
		mInitialGoalAreaYSize = mGoalArea.GetComponent<Collider2D>().bounds.size.y;
		SetInitialSize ();
	}
	void SetInitialSize()
	{

		switch (GameManager.manager.goalSize) {
		case 0:
			mInitialYModifier = mYSizeModifer = 0.9f;
			break;
		case 1:
		default:
			mInitialYModifier = mYSizeModifer = 1.2f;
			break;
		case 2:
			mInitialYModifier = mYSizeModifer = 1.5f;
			break;
		}
		SetYInitialSize (mInitialYModifier);
	}

	/// <summary>
	/// Sets the size of the initial Y scale value.
	/// </summary>
	/// <param name="yInitialSize">Y initial size.</param>
	public void SetYInitialSize(float yInitialSize)
	{
		mYSizeModifer -= mInitialYModifier;
		mInitialYModifier = yInitialSize;
		AddSizeModifier (yInitialSize);
	}

	/// <summary>
	/// Adds the size modifier to the total modifier.
	/// </summary>
	/// <param name="ySizeModifier">Y size modifier.</param>
	public void AddSizeModifier(float ySizeModifier)
	{
		if (mPostTop == null || mPostBottom == null || mGoalArea == null )
			return;
		mYSizeModifer += ySizeModifier;

		//Top post
		mPostTop.transform.localPosition = ChangeYValue(mPostTop.transform.localPosition, mInitialTopPostY * mYSizeModifer);
		//Bottom post
		mPostBottom.transform.localPosition = ChangeYValue(mPostBottom.transform.localPosition, mInitialBottomPostY * mYSizeModifer);
		//Collider
		mGoalArea.GetComponent<BoxCollider2D> ().size = ChangeYValue(mGoalArea.GetComponent<BoxCollider2D> ().size, mInitialGoalAreaYSize * mYSizeModifer);
	}

	private Vector3 ChangeYValue (Vector3 v3, float value)
	{
		v3.y = value;
		return v3;
	}


	/// <summary>
	/// Removes the size modifier from the total modifier.
	/// </summary>
	/// <param name="ySizeModifier">Y size modifier.</param>
	public void RemoveSizeModifier(float ySizeModifier) {AddSizeModifier (-ySizeModifier);}
}
