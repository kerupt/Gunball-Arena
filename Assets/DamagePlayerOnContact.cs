﻿using UnityEngine;
using System.Collections;

public class DamagePlayerOnContact : MonoBehaviour {

	[SerializeField]
	protected float mDamage;
	protected float mInitialDamage;

	void Start()
	{
		mInitialDamage = mDamage;
	}

	protected void OnTriggerEnter2D(Collider2D other)
	{
		GameObject go = other.attachedRigidbody != null ? 
			other.attachedRigidbody.gameObject :
				other.gameObject;

		if (go.tag == "Player") {
			DamagePlayer(go);
		}
	}

	protected virtual float CalculateDamage(GameObject go)
	{
		return mDamage;
	}

	protected void DamagePlayer(GameObject go)
	{
		ApplySizeModifier ();

		PlayerHealth pp = go.GetComponent<PlayerHealth>();
		if (pp == null)
			return;
		pp.TakeDamage(CalculateDamage(go));
	}

	protected void ApplySizeModifier()
	{
		BaseProjectile bp = GetComponent<BaseProjectile> ();
		if (bp == null)
			return;
		mDamage = bp.GetSizeModifier () * mInitialDamage;
	}
}
