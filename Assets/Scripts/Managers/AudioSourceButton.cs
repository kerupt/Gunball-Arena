﻿using UnityEngine;
using System.Collections;

public class AudioSourceButton : MonoBehaviour {

	public static AudioSource buttonAudioSource;

	void Awake () {
		if(buttonAudioSource == null){
			DontDestroyOnLoad(gameObject);
			buttonAudioSource = this.GetComponent<AudioSource>();
		} else if (buttonAudioSource != this){
			Destroy(gameObject);
		}
	}
}
