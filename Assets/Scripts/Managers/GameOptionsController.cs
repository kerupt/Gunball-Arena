﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOptionsController : MonoBehaviour {

	public Slider goalSlider;
	public Toggle wrapToggle;

	private int TimeLimitChangeValue = 30;
	private int ScoreLimitChangeValue = 5;
	
	public void DecrementTimeLimit(){
		PlayerPrefsManager.SetTimeLimit(PlayerPrefsManager.GetTimeLimit() - TimeLimitChangeValue);
	}
	public void IncrementTimeLimit(){
		PlayerPrefsManager.SetTimeLimit(PlayerPrefsManager.GetTimeLimit() + TimeLimitChangeValue);
	}
	
	public void DecrementScoreLimit(){
		PlayerPrefsManager.SetScoreLimit(PlayerPrefsManager.GetScoreLimit() - ScoreLimitChangeValue);
	}
	public void IncrementScoreLimit(){
		PlayerPrefsManager.SetScoreLimit(PlayerPrefsManager.GetScoreLimit() + ScoreLimitChangeValue);
		
	}

	public void SetDefaultGameOptions(){
		PlayerPrefsManager.SetWrapping(0);
		wrapToggle.isOn = false;
		PlayerPrefsManager.SetGoalSize(1);
		goalSlider.value = 1;
		PlayerPrefsManager.SetTimeLimit(120);

		PlayerPrefsManager.SetScoreLimit(15);
	}
}
