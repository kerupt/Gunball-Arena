﻿using UnityEngine;
using System.Collections;

public class SetStartVolume : MonoBehaviour {

	private MusicManager musicManager;

	// Use this for initialization
	void Start () {
		musicManager = GameObject.FindObjectOfType<MusicManager>();
		if (musicManager){

			float volume = PlayerPrefsManager.GetMasterVolume();
			Debug.Log (volume);
			if (volume > 0.25f){
				volume = 0.25f;
				PlayerPrefsManager.SetMasterVolume(0.25f);
			}
			musicManager.SetVolume(volume);
		} else {
			Debug.LogWarning("No music manager found in Start Menu, cannot set volume");
		}
	}
	

}
