﻿using UnityEngine;
using System.Collections;

public class GetWinnerLoser : MonoBehaviour {
	public GameObject winner;
	public GameObject loser;

	// Use this for initialization
	void Start () {
		GameObject winnerObject = null;
		GameObject loserObject = null;

		if (GameManager.manager.GetWinner() == 0){
			winnerObject = Instantiate(GameManager.manager.Player1ToSpawn, winner.transform.position, winner.transform.rotation) as GameObject;
			loserObject = Instantiate(GameManager.manager.Player2ToSpawn, loser.transform.position, loser.transform.rotation) as GameObject;
			loserObject.GetComponent<PlayerSpriteCooldownHandler>().SetBodyColor(GameManager.manager.mPlayer2Color);

		}else if(GameManager.manager.GetWinner() == 1){
			winnerObject = Instantiate(GameManager.manager.Player2ToSpawn, winner.transform.position, winner.transform.rotation) as GameObject;
			loserObject = Instantiate(GameManager.manager.Player1ToSpawn, loser.transform.position, loser.transform.rotation) as GameObject;
			winnerObject.GetComponent<PlayerSpriteCooldownHandler>().SetBodyColor(GameManager.manager.mPlayer2Color);

		}
		if (winnerObject == null || loserObject == null)
			throw new UnityException("Missing object in win screen");

		ReplaceSpriteRenderer (ref winner, ref winnerObject);
		ReplaceSpriteRenderer (ref loser, ref loserObject);
	}

	void ReplaceSpriteRenderer(ref GameObject render, ref GameObject obj)
	{
		SpriteRenderer rend = render.GetComponent<SpriteRenderer> ();
		SpriteRenderer rendObj = obj.transform.GetChild (1).GetChild (0).GetComponent<SpriteRenderer> ();
		rend.sprite = rendObj.sprite;
		rend.color = rendObj.color;
		obj.SetActive(false);
	}

}
