﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class CharacterScreenshotPanel : MonoBehaviour {

	public GameObject Char1;
	public GameObject Char2;
	public GameObject Char3;
	public GameObject Char4;
	
	
	// Update is called once per frame
	void Update () {
		if (EventSystem.current.currentSelectedGameObject == Char1){
			gameObject.GetComponent<Image>().sprite = Char1.GetComponent<Image>().sprite;
		} else if (EventSystem.current.currentSelectedGameObject == Char2){
			gameObject.GetComponent<Image>().sprite = Char2.GetComponent<Image>().sprite;
		} else if (EventSystem.current.currentSelectedGameObject == Char3){
			gameObject.GetComponent<Image>().sprite = Char3.GetComponent<Image>().sprite;
		} else if (EventSystem.current.currentSelectedGameObject == Char4){
			gameObject.GetComponent<Image>().sprite = Char4.GetComponent<Image>().sprite;
		} 
		
	}
}
