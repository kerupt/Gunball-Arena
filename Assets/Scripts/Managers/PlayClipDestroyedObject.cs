﻿using UnityEngine;
using System.Collections;

public class PlayClipDestroyedObject : MonoBehaviour {

	public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 position)
	{
		GameObject temp = new GameObject ("Temp audio source");
		temp.transform.position = position;
		AudioSource tempAudio = temp.AddComponent<AudioSource> ();
		tempAudio.clip = clip;
		tempAudio.Play ();
		Destroy (temp, clip.length);
		return tempAudio;
	}

	public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 position, float loopDuration)
	{
		GameObject temp = new GameObject ("Temp audio source");
		temp.transform.position = position;
		AudioSource tempAudio = temp.AddComponent<AudioSource> ();
		tempAudio.clip = clip;
		tempAudio.Play ();
		tempAudio.loop = true;
		Destroy (temp, loopDuration);
		return tempAudio;
	}
	
}
