﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnableAfterXSeconds : MonoBehaviour {
	public GameObject obj;

	public float DelaySeconds;
	private float timeElapsed = 0f;
	private bool enabled = false;

	// Use this for initialization
	void Start () {
		obj.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (!enabled){
			timeElapsed += Time.deltaTime;

			if (timeElapsed > DelaySeconds){
				obj.SetActive(true);
				enabled = true;
			}
		}
	}
}
