﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public float autoLoadNextLevelAfter;

	void Start(){
		if (autoLoadNextLevelAfter <= 0){
			// Dont load next level
		}else {
			Invoke("LoadNextLevel", autoLoadNextLevelAfter);
		}
	}

	public void LoadLevel (string name){
		Debug.Log("New Level Load: " + name);
		Application.LoadLevel(name);
	}

	public void QuitRequest (){
		Debug.Log("Quit Game");
		Application.Quit();
	}

	public void LoadNextLevel(){
		Application.LoadLevel (Application.loadedLevel + 1);
	}


}
