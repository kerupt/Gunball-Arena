﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayButtonSound : MonoBehaviour {

	private Button MyButton; 
	
	void Start() { 
		MyButton = gameObject.GetComponent<Button>();
		MyButton.onClick.AddListener(() => { PlaySound(); });
	}
	
	void PlaySound(){
		AudioSourceButton.buttonAudioSource.Play();
	}
}
