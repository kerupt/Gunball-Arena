﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class MapScreenshotPanel : MonoBehaviour {

	public GameObject Map1Button;
	public GameObject Map2Button;
	public GameObject Map3Button;
	public GameObject Map4Button;

	public Sprite Stage1;
	public Sprite Stage2;
	public Sprite Stage3;
	public Sprite Stage4;
	
	// Update is called once per frame
	void Update () {
		if (EventSystem.current.currentSelectedGameObject == Map1Button){
			gameObject.GetComponent<Image>().sprite = Stage1;
		} else if (EventSystem.current.currentSelectedGameObject == Map2Button){
			gameObject.GetComponent<Image>().sprite = Stage2;
		} else if (EventSystem.current.currentSelectedGameObject == Map3Button){
			gameObject.GetComponent<Image>().sprite = Stage3;
		} else if (EventSystem.current.currentSelectedGameObject == Map4Button){
			gameObject.GetComponent<Image>().sprite = Stage4;
		} 

	}
}
