﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WrappingToggle : MonoBehaviour {

	private Toggle toggle;

	void Start(){
		toggle = gameObject.GetComponent<Toggle>();
		toggle.onValueChanged.AddListener (delegate {ValueChangeCheck ();});
		if (PlayerPrefsManager.GetWrapping() == 0){
			toggle.isOn = false;
		} else {
			toggle.isOn = true;
		}
	}

	void ValueChangeCheck () {
		if (toggle.isOn){
			PlayerPrefsManager.SetWrapping(1);
			GameManager.manager.wrapping = 1;
			Debug.Log ("Wrapping set to: " + PlayerPrefsManager.GetWrapping());
		}else {
			PlayerPrefsManager.SetWrapping(0);
			GameManager.manager.wrapping = 0;
			Debug.Log ("Wrapping set to: " + PlayerPrefsManager.GetWrapping());
		}
	}
}
