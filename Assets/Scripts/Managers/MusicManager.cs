﻿using UnityEngine;
using System.Collections;
using System;

public class MusicManager : MonoBehaviour {

	[Serializable]
	public class MusicLevelDictionary
	{
		[SerializeField]
		string mElementName; 
		[SerializeField]
		bool mRandomize;

		public AudioClip[] levelMusicChangeArray;

		public AudioClip this[int ID]
		{
			get { 
				if (mRandomize)
				{
					ID = (int)(UnityEngine.Random.value * levelMusicChangeArray.Length);
					ID = ID >= levelMusicChangeArray.Length ? levelMusicChangeArray.Length : ID; 
				}
				return (ID >= 0 && ID < levelMusicChangeArray.Length) ? levelMusicChangeArray[ID] : null; 
			}
		}

		/// <summary>
		/// If ever we decide to implement a music room.
		/// </summary>
		/// <returns>The audio clip.</returns>
		/// <param name="ID">I.</param>
		/// <param name="randomize">If set to <c>true</c> randomize.</param>
		public AudioClip GetAudioClip(int ID, bool randomize = false)
		{
			bool r = mRandomize;
			mRandomize = randomize;
			AudioClip ac = this [ID];
			mRandomize = r;
			return ac;
		}
	}

	[SerializeField]
	MusicLevelDictionary[] levelMusicChangeArray;

	private AudioSource audioSource;

	void Awake(){
		DontDestroyOnLoad(gameObject);
	}

	void Start(){
		audioSource = GetComponent<AudioSource>();
	}

	void OnLevelWasLoaded(int level){
		AudioClip previousMusic = audioSource.clip;

		AudioClip thisLevelMusic = null;
		if (level < levelMusicChangeArray.Length)
		 	thisLevelMusic = levelMusicChangeArray[level][0];

		if (thisLevelMusic && thisLevelMusic != previousMusic){
			audioSource.clip = thisLevelMusic;
			audioSource.loop = true;
			audioSource.Play ();
		}
		if (level == 10){  //final level
			audioSource.loop = false;
		}
	}

	public void SetVolume(float volume){
		audioSource.volume = volume;
	}
}
