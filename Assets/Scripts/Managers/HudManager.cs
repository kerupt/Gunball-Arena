﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HudManager : MonoBehaviour 
{
	private static HudManager _instance;
	public static HudManager Instance 
	{
		get
		{
			return _instance;
		}

		private set 
		{
			_instance = value;
		}
	}

	public Text p1Name;
	public Text p1Score;
	public Text p2Score;
	public Text p2Name;

	[SerializeField]
	WarningText mWarningText;
	public WarningText WarningText { get { return mWarningText; } }
	[SerializeField]
	FlashScreen mFlashScreen;
	public FlashScreen FlashScreen { get { return mFlashScreen; } }
	[SerializeField]
	WarningText mTieBreakerText;
	public WarningText TieBreakerText { get { return mTieBreakerText; } } 

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	void OnEnable(){
		GameManager.OnScore += CheckScores;
	}

	void OnDisable(){
		GameManager.OnScore -= CheckScores;
	}

	// Use this for initialization
	void Start () {
		p1Name = p1Name.GetComponent<Text>();
		p1Name.text = GameManager.manager.getP1Name();
		p1Score = p1Score.GetComponent<Text>();
		p2Score = p2Score.GetComponent<Text>();
		p2Name = p2Name.GetComponent<Text>();
		p2Name.text = GameManager.manager.getP2Name();
	}
	
	void CheckScores(){
		p1Score.text = GameManager.manager.GetPlayerScore(1).ToString("00");
		p2Score.text = GameManager.manager.GetPlayerScore(2).ToString("00");
	}

}
