﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
	private float timeLimit;
	private float restSeconds ;
	private float roundedRestSeconds; 
	private int displaySeconds; 
	private int displayMinutes;
	private float gameStartCountdown = 1.8f;
	private int countdownCount = 0;
	private Text myTextTimer;
	private Text myTextCountdown;
	
	public GameObject timerTextObject;
	public GameObject countdownObject;
	public AudioClip countdown;

	// Use this for initialization
	void Start () {
		myTextTimer = timerTextObject.GetComponent<Text>();
		myTextCountdown = countdownObject.GetComponent<Text>();
		timeLimit = PlayerPrefsManager.GetTimeLimit();
		displaySeconds = (int)timeLimit % 60;
		displayMinutes = (int)timeLimit / 60;
		string text = string.Format ("{0:00}:{1:00}", displayMinutes, displaySeconds);
		myTextTimer.text = text;

		// PLAY SOUND CLIP 3
		AudioSource.PlayClipAtPoint(countdown, transform.position);
		Debug.Log ("3!");
	}
	
	// Update is called once per frame
	void Update () {
		if (gameStartCountdown > 0){
			gameStartCountdown -= Time.deltaTime;
			if (gameStartCountdown <= 1.2 && countdownCount == 0){
				// PLAY SOUND CLIP 2
				myTextCountdown.text = "2";
				Debug.Log ("2!");
				countdownCount++;
			}else if(gameStartCountdown <= 0.6 && countdownCount == 1){
				// PLAY SOUND CLIP 1
				myTextCountdown.text = "1";
				Debug.Log ("1!");
				countdownCount++;
			}

		} else {
			if (gameStartCountdown >= -0.6f){
				gameStartCountdown -= Time.deltaTime;
			}
			if(gameStartCountdown <= 0 && countdownCount == 2){
				// PLAY SOUND CLIP START GAME / WHISTLE
				myTextCountdown.text = "GO!";
				Debug.Log ("GO!");
				countdownCount++;
			}

			if (gameStartCountdown <= -0.6f){
				myTextCountdown.text = "";
				myTextCountdown.GetComponent<Animator>().enabled = false;
			}

			timeLimit -= Time.deltaTime;
			restSeconds = timeLimit;
			GameManager.manager.timeLimit = timeLimit;
			roundedRestSeconds = Mathf.CeilToInt(restSeconds);
			displaySeconds = (int)roundedRestSeconds % 60;
			displayMinutes = (int)roundedRestSeconds / 60; 
			
			if (displayMinutes <= 0){
				displayMinutes = 0;
			}
			
			if (displaySeconds <= 0){
				displaySeconds = 0;
			}

			string text = string.Format ("{0:00}:{1:00}", displayMinutes, displaySeconds);
			myTextTimer.text = text;
		}
	}
}
