﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GoalSizeSlider : MonoBehaviour {

	private Slider slider;
	
	void Start(){
		slider = gameObject.GetComponent<Slider>();
		slider.value = PlayerPrefsManager.GetGoalSize();
		slider.onValueChanged.AddListener (delegate {ValueChangeCheck ();});
	}
	
	void ValueChangeCheck () {
		if (slider.value == 0){
			PlayerPrefsManager.SetGoalSize(0);
			Debug.Log ("Goal size set to: " + PlayerPrefsManager.GetGoalSize());
		}else if (slider.value == 1){
			PlayerPrefsManager.SetGoalSize(1);
			Debug.Log ("Goal size set to: " + PlayerPrefsManager.GetGoalSize());
		}else if (slider.value == 2){
			PlayerPrefsManager.SetGoalSize(2);
			Debug.Log ("Goal size set to: " + PlayerPrefsManager.GetGoalSize());
		}
	}
}
