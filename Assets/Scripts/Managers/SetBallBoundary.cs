﻿using UnityEngine;
using System.Collections;

public class SetBallBoundary : MonoBehaviour {
	public GameObject ballTopBoundary;
	public GameObject ballBottomBoundary;
	// Use this for initialization
	void Start () {
		if(GameManager.manager.wrapping == 0){
			ballTopBoundary.SetActive(true);
			ballBottomBoundary.SetActive(true);
		}else {
			ballTopBoundary.SetActive(false);
			ballBottomBoundary.SetActive(false);
		}
	}
}
