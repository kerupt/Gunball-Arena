﻿using UnityEngine;
using System;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public static GameManager manager;
	
	private CharacterDictionary mCharacterDictionary;
	private StageDictionary mStageDictionary;
	
	private static bool gameStarted;
	private static bool gameOver;
	private const int DEFAULT_WRAPPING = 0;
	public int wrapping;    				// 0 = no wrap, 1 = wrapping
	private const int DEFAULT_GOAL_SIZE = 1;
	public int goalSize;					// 0 = small, 1 = normal (default), 2 = large
	private const int DEFAULT_SCORE_LIMIT = 15;
	public int scoreLimit;
	private const float DEFAULT_TIME_LIMIT = 120;
	public float timeLimit;

	[SerializeField]
	float mBallScoreDelay = 1f;
	[SerializeField]
	float mBallStartDelay = 2.3f;
	
	[SerializeField]
	private GameObject _player1ToSpawn;
	public GameObject Player1ToSpawn
	{
		get
		{
			return _player1ToSpawn;
		}
		
		set
		{
			_player1ToSpawn = value;
		}
	}
	
	[SerializeField]
	private GameObject _player2ToSpawn;
	public GameObject Player2ToSpawn
	{
		get
		{
			return _player2ToSpawn;
		}
		
		set
		{
			_player2ToSpawn = value;
		}
	}
	
	private GameObject mPlayer1;
	public GameObject Player1 { get { return mPlayer1; } }
	private GameObject mPlayer2;
	public GameObject Player2 { get { return mPlayer2; } }

	public string getP1Name(){
		return _player1ToSpawn.name;
	}

	public string getP2Name(){
		return _player2ToSpawn.name;
	}

	public delegate void ScoreAction();
	public static event ScoreAction OnScore;

	//public AudioClip scoringSound;

	private int[] mPlayerScores = new int[2];
	public void GoalScored(int playerNumber)
	{
		if (gameOver)
			return;

		//AudioSource.PlayClipAtPoint(scoringSound, transform.position);
		int tPlayerNumber = GetPlayerNumber (playerNumber);
		++mPlayerScores [tPlayerNumber];
		if (OnScore != null)
			OnScore ();
		
		//Check if match end
		if (GetPlayerScore (playerNumber) >= scoreLimit || mIsTiebreak)
			EndMatch ();
		//No: Spawn ball
		if (!gameOver){
			StartCoroutine(SpawnBallAfterSeconds(mBallScoreDelay, true)); 
//			CurrentlyLoadedStage.SpawnBall();
		}
	}
	public int GetPlayerScore(int playerNumber)
	{
		return mPlayerScores [GetPlayerNumber(playerNumber)];
	}
	private int GetPlayerNumber(int playerNumber)
	{
		playerNumber -= 1;
		CheckPlayerWithinLimits (playerNumber);
		return playerNumber;
	}
	private void CheckPlayerWithinLimits(int num)
	{
		if (num < 0 || 
		    num > mPlayerScores.Length){
			 throw new IndexOutOfRangeException (String.Format ("Player number must be between 0 and {0}", mPlayerScores.Length));
	
		}
	}
	
	Stage mCurrentlyLoadedStage;
	public Stage CurrentlyLoadedStage { get { return mCurrentlyLoadedStage; } private set { mCurrentlyLoadedStage = value; } }
	[SerializeField]
	Stage mStageToLoad;
	
	void Awake () 
	{
		if(manager == null)
		{
			DontDestroyOnLoad(gameObject);
			initializeInstance();
		} 
		else if (manager != this)
		{
			Destroy(gameObject);
		}
	}
	
	void initializeInstance()
	{
		manager = this;
		mCharacterDictionary = GetComponent<CharacterDictionary>();
		mStageDictionary = GetComponent<StageDictionary>();
	}
	
	void Start()
	{
		gameStarted = false;
		gameOver = false;
		// Uncomment when game is ready to deploy
		// Cursor.visible = false;
		scoreLimit = PlayerPrefsManager.GetScoreLimit();
		timeLimit = PlayerPrefsManager.GetTimeLimit();
		wrapping = PlayerPrefsManager.GetWrapping();
		goalSize = PlayerPrefsManager.GetGoalSize();
		
		// This runs only 1st time game has been run
		if (!PlayerPrefsManager.ValidateScoreLimit(scoreLimit) || !PlayerPrefsManager.ValidateTimeLimit(timeLimit))
		{
			scoreLimit = DEFAULT_SCORE_LIMIT;
			timeLimit = DEFAULT_TIME_LIMIT;
			wrapping = DEFAULT_WRAPPING;
			goalSize = DEFAULT_GOAL_SIZE;
		}
		PlayerPrefsManager.SetScoreLimit(scoreLimit);
		PlayerPrefsManager.SetTimeLimit(timeLimit);
		PlayerPrefsManager.SetGoalSize(goalSize);
		PlayerPrefsManager.SetWrapping(wrapping);
	}


	public float gameEndDelay = 3.5f;
	private float gameEndTimer = 0;
	void Update () 
	{
		if (!mIsTiebreak && timeLimit <= 0){
			EndMatch ();
		}
		if (gameOver){
			gameEndTimer += Time.deltaTime;
			if (gameEndTimer >= gameEndDelay){
				gameEndTimer = 0;
				gameOver = false;
				Application.LoadLevel("06 Win");
			}
		}
			
	}
	
	public void StartMatch()
	{
		//Initialize scores
		for (int i = 0; i < mPlayerScores.Length; i++) {
			mPlayerScores[i] = 0;
		}
		timeLimit = PlayerPrefsManager.GetTimeLimit();
		scoreLimit = PlayerPrefsManager.GetScoreLimit();
		wrapping = PlayerPrefsManager.GetWrapping();
		goalSize = PlayerPrefsManager.GetGoalSize();
		gameStarted = true;
		gameOver = false;
		
		loadStage();
		initializeStage();
	}

	public Color mPlayer2Color { get; private set;}
	private void initializeStage()
	{
		if (CurrentlyLoadedStage != null)
		{
			mPlayer1 = CurrentlyLoadedStage.SpawnPlayer(1, Player1ToSpawn);
			mPlayer2 = CurrentlyLoadedStage.SpawnPlayer(2, Player2ToSpawn);
			if (mPlayer1.name.Equals(mPlayer2.name))
			{
				mPlayer2.GetComponent<PlayerSpriteCooldownHandler>().RandomizeBodyColor();
			}
			mPlayer2Color = mPlayer2.GetComponent<PlayerSpriteCooldownHandler>().GetBodyColor();

			StartCoroutine(SpawnBallAfterSeconds(mBallStartDelay)); 
//			CurrentlyLoadedStage.SpawnBall();
		}
		else
		{
			Debug.Log ("Error: No Stage is loaded");
		}
	}
	
	private int winner;
	public int GetWinner(){
		return winner;
	}
	bool mIsTiebreak = false; 

	void EndMatch()
	{
		if (mIsTiebreak) {
			StopCoroutine ("TieBreakerBallSpawner");
			mIsTiebreak = false;
		}

		//Tie case
		if (mPlayerScores [0] == mPlayerScores [1]) {
			StartCoroutine("TieBreakerBallSpawner");
			HudManager.Instance.TieBreakerText.DisplayWarning("Tie breaker");
			mIsTiebreak = true;
			return;
		}
		
		if (!gameOver && gameStarted){
			if (mPlayerScores[0] > mPlayerScores[1]){
				winner = 0;
			}else if (mPlayerScores[1] > mPlayerScores[0]){
				winner = 1;
			}
			gameStarted = false;
			gameOver = true;
			HudManager.Instance.TieBreakerText.DisplayWarning("GAME OVER");
		}
	}
	
	public void SetPlayerPrefabToSpawn(int playerIndex, string characterID)
	{
		GameObject playerPrefabToSpawn = mCharacterDictionary[characterID];
		switch(playerIndex)
		{
		case 1:
			Player1ToSpawn = playerPrefabToSpawn;
			break;
		case 2:
			Player2ToSpawn = playerPrefabToSpawn;
			break;
		default:
			Debug.Log ("Invalid player index");
			break;
		}
	}
	
	public void SetStagePrefabToSpawn(string stageID)
	{
		mStageToLoad = mStageDictionary[stageID];
	}
	
	void loadStage()
	{
		if (mStageToLoad != null)
		{
			CurrentlyLoadedStage = Instantiate(mStageToLoad, Vector3.zero, Quaternion.identity) as Stage;
			Debug.Log (CurrentlyLoadedStage.name);
		}
	}

	IEnumerator SpawnBallAfterSeconds(float seconds, bool random = false)
	{
		yield return new WaitForSeconds (seconds);
		if (random)
			CurrentlyLoadedStage.SpawnBall(Vector2.zero, new Vector2(-0.5f, 0.5f), new Vector2(-2f, 2f));
		else 
			CurrentlyLoadedStage.SpawnBall();
	}

	[SerializeField]
	float mTieBreakerBallSpawnerDelay = 1f; //Spawn a new ball every x seconds
	IEnumerator TieBreakerBallSpawner()
	{
		while (true) {
			yield return new WaitForSeconds(mTieBreakerBallSpawnerDelay);
			CurrentlyLoadedStage.SpawnBall(Vector2.zero, true, true);
		}
	}
}
