﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsManager : MonoBehaviour {

	const string MASTER_VOLUME_KEY = "master_volume";	//float
	const string WRAPPING_KEY = "wrapping";				//int 
	const string GOAL_SIZE_KEY = "goal_size";			//int
	const string TIME_LIMIT_KEY = "time_limit";   		//float
	const string SCORE_LIMIT_KEY = "score_limit";		//int

	// VOLUME
	public static void SetMasterVolume(float volume){
		if (volume >= 0f && volume <= 0.25f){
			PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
		} else {
			Debug.LogError("Master volume out of range");
		}
 	}

	public static float GetMasterVolume(){
		return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
	}

	// WRAPPING
	public static void SetWrapping(int wrapping){
		if (wrapping == 0 || wrapping == 1){
			PlayerPrefs.SetInt(WRAPPING_KEY, wrapping);
		}else {
			Debug.Log("Wrapping out of range");
		}
	}
	
	public static int GetWrapping(){
		return PlayerPrefs.GetInt(WRAPPING_KEY);
	}

	// GOAL SIZE
	public static void SetGoalSize(int goalSize){
		if (goalSize >= 0 && goalSize <= 2){
			PlayerPrefs.SetInt(GOAL_SIZE_KEY, goalSize);
		} else {
			Debug.Log("Goal size out of range");
		}
	}

	public static int GetGoalSize(){
		return PlayerPrefs.GetInt(GOAL_SIZE_KEY);
	}

	// TIME LIMIT
	public static void SetTimeLimit(float timeLimit){
		if (timeLimit >= 60 && timeLimit <= 600){
			PlayerPrefs.SetFloat(TIME_LIMIT_KEY, timeLimit);
		} else {
			Debug.Log("Time limit out of range");
		}
	}

	public static float GetTimeLimit(){
		return PlayerPrefs.GetFloat(TIME_LIMIT_KEY);
	}

	public static bool ValidateTimeLimit(float timeLimit){
		if (timeLimit >= 60 && timeLimit <= 600){
			return true;
		} else {
			return false;
		}
	}

	// SCORE LIMIT
	public static void SetScoreLimit(int scoreLimit){
		if (scoreLimit >= 10 && scoreLimit <= 50){
			PlayerPrefs.SetInt(SCORE_LIMIT_KEY, scoreLimit);
		} else {
			Debug.Log("Score limit out of range");
		}
	}
	
	public static int GetScoreLimit(){
		return PlayerPrefs.GetInt(SCORE_LIMIT_KEY);
	}

	public static bool ValidateScoreLimit(int scoreLimit){
		if (scoreLimit >= 10 && scoreLimit <= 50){
			return true;
		} else {
			return false;
		}
	}
}