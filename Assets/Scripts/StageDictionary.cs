﻿using UnityEngine;
using System.Collections;

public class StageDictionary : MonoBehaviour 
{

	[System.Serializable]
	public class StageAssetInformation
	{
		[SerializeField]
		string mStageID;
		public string StageID { get { return mStageID; } }
		[SerializeField]
		Stage mStagePrefab;
		public Stage StagePrefab { get { return mStagePrefab; } }
	}
	
	[SerializeField]
	StageAssetInformation[] mStages;
	
	public Stage this[string ID]
	{
		get { return getStagePrefab(ID); }
	}
	
	Stage getStagePrefab(string stageID)
	{
		Stage stagePrefab = null;
		for (int i = 0; i < mStages.Length; i++)
		{
			if (mStages[i].StageID.Equals(stageID))
			{
				stagePrefab = mStages[i].StagePrefab;
				break;
			}
		}
		
		return stagePrefab;
	}
}
