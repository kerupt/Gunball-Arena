﻿using UnityEngine;
using System.Collections;

public abstract class BaseProjectile : MonoBehaviour {

	[SerializeField]
	protected float mMoveSpeed;
	protected bool ignorePlayerCollision = true; //Ignore collisions with the player who shot

	public float MoveSpeed
	{
		get{return mMoveSpeed;}
		set{mMoveSpeed = value;}
	}


	protected int mShotFrom;
	protected GameObject mPlayerShotFrom;

	protected float mSizeModifier = 1;
	public float GetSizeModifier (){
		return mSizeModifier;
	}

	public void SetShotFrom(int i)
	{
		mShotFrom = i;
	}

	public void SetShotFrom(GameObject player)
	{
		if (player.tag != "Player")
			return;
		mPlayerShotFrom = player;
		ModifyScale ();
		SetShotFrom (player.GetComponent<PlayerInput> ().GetPlayerNumber ());

		if (ignorePlayerCollision)
			DisablePlayerCollision ();
	}

	protected void DisablePlayerCollision()
	{
		Collider2D[] playerCollider = mPlayerShotFrom.GetComponentsInChildren<Collider2D> ();
		if (playerCollider == null)
			return;
		foreach (var col in playerCollider) {
			Physics2D.IgnoreCollision (col, GetComponent<Collider2D> ());
		}
	}

	protected void ModifyScale()
	{
		if (mPlayerShotFrom == null)
			return;
		mSizeModifier = mPlayerShotFrom.GetComponent<PlayerInput> ().mSizeModifier;
		gameObject.transform.localScale *= mSizeModifier;
	}
}
