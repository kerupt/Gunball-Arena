﻿using UnityEngine;
using System.Collections;

public class Rocket : BaseProjectile {

	[SerializeField]
	GameObject mExplosionPrefab;

	void Start()
	{
		ignorePlayerCollision = true;
	}

	void Awake()
	{
		ignorePlayerCollision = true;
//		DisablePlayerCollision ();
	}

	void Update () {
		transform.Translate (Vector2.up * Time.deltaTime * mMoveSpeed);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		GameObject go = other.attachedRigidbody != null ? 
			other.attachedRigidbody.gameObject :
				other.gameObject;
		if (go.tag == "Player" &&
			go.GetComponent<PlayerInput> ().GetPlayerNumber () == mShotFrom)
				return;
		Explode();
	}

	void Explode()
	{
		GameObject explosion = (Instantiate (mExplosionPrefab, transform.position, transform.rotation) as GameObject);
		explosion.GetComponent<Explosion>().SetShotFrom(mShotFrom);
		CircleCollider2D cc2d = explosion.GetComponent<CircleCollider2D> ();
		cc2d.radius *= mSizeModifier;

		Destroy (gameObject);
	}
}
