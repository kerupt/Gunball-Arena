﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D))]
public class Explosion : MonoBehaviour {
	int mShotFrom;

	[SerializeField]
	float mExplosionDuration;

	float mRadius;

	[SerializeField]
	AudioClip explosionSound;

	void Start()
	{
		PlayClipDestroyedObject.PlayClipAtPoint(explosionSound, transform.position);
		mRadius = GetComponent<CircleCollider2D> ().radius;
		Destroy (gameObject, mExplosionDuration);
	}

	public void SetShotFrom(int i)
	{
		mShotFrom = i;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Ball")
			other.gameObject.GetComponent<LastPlayerHitBy> ().PlayerHit (mShotFrom);
		else if (other.attachedRigidbody != null &&
		         other.attachedRigidbody.gameObject.tag == "Player")
		{
			ApplyPlayerForce(other.attachedRigidbody.gameObject);
		}
	}	

	void ApplyPlayerForce(GameObject go)
	{
		//Get direction with explosion center
		Vector2 dir = go.transform.position - transform.position;

		Rigidbody2D body = go.GetComponent<Rigidbody2D> ();
		if (body == null)
			return;

		PointEffector2D pe2d = GetComponent<PointEffector2D> ();
		float force = 100;
		if (pe2d != null)
			force = pe2d.forceMagnitude;

		force -= ((force * dir.magnitude / mRadius));

		if (force < 0.0f)
			force = 0.0f;

		body.AddForce (dir.normalized * force, ForceMode2D.Impulse);
	}

	//
//	void OnTriggerEnter2D(Collider2D other)
//	{
//		//Get direction with explosion center
//		Vector2 dir = other.transform.position - transform.position;
//
//		Rigidbody2D body = other.GetComponent<Rigidbody2D> ();
//		if (body == null)
//			return;
//
//
//		float force = mForce - (mForce * dir.sqrMagnitude / (mRadius * mRadius));
//		if (force < 0.0f)
//			force = 0.0f;
//
//		body.AddForce (dir.normalized * force, ForceMode2D.Impulse);
//	}
}
