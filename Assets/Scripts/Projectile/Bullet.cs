﻿using UnityEngine;
using System.Collections;

public class Bullet : BaseProjectile {

	[SerializeField]
	float mHitBallSpeed;
  
	void Update () {
//		Vector3 dir = new Vector3 (0, 0, 0);
//		dir.x = (float)Mathf.Cos(Mathf.Deg2Rad(transform.rotation.));


		transform.Translate (Vector2.up * Time.deltaTime * mMoveSpeed);
	}


	[SerializeField]
	float mOppositeDirectionForceReduction = 1.5f;
	[SerializeField]
	float mLessThanZeroForceReduction = 1.2f; 
	void OnTriggerEnter2D(Collider2D collider)
	{
		GameObject other = collider.gameObject;
		if (collider.attachedRigidbody != null) 
		{
			other = collider.attachedRigidbody.gameObject;
		}

		if (other.gameObject.layer == LayerMask.NameToLayer("StageBoundary"))
		{
			Destroy(gameObject);
		}
		if (other.gameObject.tag == "Ball")
		{ 
			Vector2 dir = CalculateDirection();

			//Reduce force from opposite side
			Vector2 vel = other.GetComponent<Rigidbody2D> ().velocity;
			float hitForce = mHitBallSpeed;
			float dot = Vector2.Dot (vel, dir);
			if (dot < -0.5)
				hitForce /= mOppositeDirectionForceReduction;
			else if (dot < 0)
				hitForce /= mLessThanZeroForceReduction;

			other.gameObject.GetComponent<Ball>().AddForce(dir * hitForce);
			other.gameObject.GetComponent<LastPlayerHitBy>().PlayerHit(mShotFrom);
//			other.gameObject.GetComponent<Ball>().AddVelocity(dir * mHitBallSpeed);
			Destroy(gameObject);
		}
		if (other.gameObject.tag == "Player" && 
		    	other.gameObject.GetComponent<PlayerInput>().GetPlayerNumber() != mShotFrom) {

			Vector2 dir = CalculateDirection();
			other.gameObject.GetComponent<Rigidbody2D>().AddForce(dir * mHitBallSpeed);
			Destroy(gameObject);
		}
	}

	Vector2 CalculateDirection()
	{
		float sinAngle = -Mathf.Sin(Mathf.Deg2Rad * transform.localEulerAngles.z);
		float cosAngle = Mathf.Cos(Mathf.Deg2Rad * transform.localEulerAngles.z);
		return new Vector2 (sinAngle, cosAngle);
	}
}
