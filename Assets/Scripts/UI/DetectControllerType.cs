﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DetectControllerType : MonoBehaviour 
{
	[SerializeField]
	Sprite keyboardImage;
	[SerializeField]
	Sprite controllerImage;

	Image controlsImage;

	void Start ()
	{
		controlsImage = GetComponent<Image>();
	}

	void Update () 
	{	
		if (checkForControllerInput())
		{
			controlsImage.sprite = controllerImage;
		}

		if (checkForKeyboardInput())
		{
			controlsImage.sprite = keyboardImage;
		}

	}

	bool checkForKeyboardInput()
	{
		return (Input.anyKey && !string.IsNullOrEmpty(Input.inputString));
	}

	bool checkForControllerInput()
	{
		bool inputDetected = false;
		int[] numbers = new int[]{0,1,2,3,4,5,6,7,8,9};
		foreach (float buttonNumber in numbers) 
		{
			if(Input.GetKeyDown("joystick 1 button " + buttonNumber) || Input.GetKeyDown("joystick 2 button " + buttonNumber))
			{
				inputDetected = true;
			}
		}

		return inputDetected;
	}
}
