﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Goal : MonoBehaviour {
	[SerializeField]
	int mPlayerNumber = 1;

	int mOpponentScore = 0;

	GameObject mBall;

	public delegate void ScoredGoalEventHandler(Goal sender);
	public ScoredGoalEventHandler ScoredGoalEvent;

	[SerializeField]
	float mAnimationTime = 1f;
	[SerializeField]
	Animator[] mAnimators;


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Ball") {
			//Keep the ball reference
			mBall = other.gameObject;
			//Hide the ball
			other.gameObject.SetActive(false);

			GoalScored();
		}
	}

	void GoalScored()
	{
		//Tell listeners that a goal has been scored
		if (ScoredGoalEvent != null)
			ScoredGoalEvent (this);

		//TODO Compare goal player with last hit
		bool sameGoal = false;
		if (mPlayerNumber == mBall.GetComponent<LastPlayerHitBy>().GetLastPlayerHitBy())
			sameGoal = true;

		PlaySounds (sameGoal);

		StartCoroutine(PlayAnimation ());

		ChangeScores ();
	}


	public AudioClip scoringSound;
	public AudioClip ownGoalSound;
	void PlaySounds(bool sameGoal)
	{
		//TODO Play goal scored sound
		
		//TODO Play CHEER or BOO
		if (sameGoal){
			AudioSource.PlayClipAtPoint(ownGoalSound, transform.position);
		}

		AudioSource.PlayClipAtPoint(scoringSound, transform.position);

	
	}


	IEnumerator PlayAnimation()
	{
		if (!(mAnimators == null || mAnimators.Length == 0)) {
			foreach (var post in mAnimators) {
				post.SetBool("Scored", true);
			}
			yield return new WaitForSeconds (mAnimationTime);
			foreach (var post in mAnimators) {
				post.SetBool("Scored", false);
			}
		}
		yield return null;
	}

	void ChangeScores()
	{
		//TODO Change scores
		//if (mOpponentScoreLabel == null)
			//return;
		//mOpponentScoreLabel.text = (++mOpponentScore).ToString("00");
		if (mPlayerNumber == 1){
			GameManager.manager.GoalScored(2);
		}else if(mPlayerNumber == 2){
			GameManager.manager.GoalScored(1);
		}

		//Get rid of current ball
		Destroy (mBall);

	}

	[SerializeField]
	GameObject mBallPrefab;
}
