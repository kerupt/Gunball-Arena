﻿using UnityEngine;
using System.Collections;

public class CharacterSelectButtonHandler : MonoBehaviour 
{
	[SerializeField]
	int playerNumber = 1;

	public void SetPlayerCharacter(string characterID)
	{
		GameManager.manager.SetPlayerPrefabToSpawn(playerNumber, characterID);
	}
}
