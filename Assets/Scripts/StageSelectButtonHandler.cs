﻿using UnityEngine;
using System.Collections;

public class StageSelectButtonHandler : MonoBehaviour 
{
	public void SetStage(string stageID)
	{
		GameManager.manager.SetStagePrefabToSpawn(stageID);
	}
}
