﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stage : MonoBehaviour 
{
	[SerializeField]
	Transform[] mPlayerSpawns;

	[SerializeField]
	Transform ballSpawn;

	[SerializeField]
	Ball ballPrefab;

	[SerializeField]
	Goal mTeam1Goal;
	[SerializeField]
	Goal mTeam2Goal;

	int activeBallCount = 0;

	public Goal Team1Goal { get { return mTeam1Goal; } private set { mTeam1Goal = value; }}
	public Goal Team2Goal { get { return mTeam2Goal; } private set { mTeam2Goal = value; }}

	void Start()
	{
		GameManager.OnScore += decrementBallCount;
	}
		
	public GameObject SpawnPlayer(int playerNumber, GameObject playerPrefab)
	{
		GameObject spawnedPlayer = null;
		int playerIndex = playerNumber - 1;
		if (playerIndex < mPlayerSpawns.Length)
		{
			Transform spawnPosition = mPlayerSpawns[playerIndex];
			if (spawnPosition != null)
			{
				spawnedPlayer = Instantiate(playerPrefab, spawnPosition.position, Quaternion.identity) as GameObject;
			}
		}
		spawnedPlayer.GetComponent<PlayerInput> ().SetPlayerNumber (playerNumber);
		return spawnedPlayer;
	}

	public void decrementBallCount()
	{
		activeBallCount--;
	}

	public Ball SpawnBall(bool isExtra = false)
	{
		Ball newBall = null;

		if (activeBallCount == 0 || isExtra)
		{
			newBall = Instantiate(ballPrefab, ballSpawn.position, Quaternion.identity) as Ball;
			activeBallCount++;
		}

		return newBall;
	}

	/// <summary>
	/// Spawns the ball in a direction.
	/// </summary>
	/// <returns>The ball</returns>
	/// <param name="isExtra">If set to <c>true</c> is extra.</param>
	/// <param name="direction">Direction. set to Vector2.zero will generate a random direction</param>
	public Ball SpawnBall(Vector2 direction, bool isExtra = false, bool randomColor = false)
	{
		return SpawnBall(direction, new Vector2(-2f,2f), new Vector2(-2f,2f), isExtra, randomColor);
	}

	/// <summary>
	/// Spawns the ball in a direction.
	/// </summary>
	/// <returns>The ball</returns>
	/// <param name="isExtra">If set to <c>true</c> is extra.</param>
	/// <param name="direction">Direction. set to Vector2.zero will generate a random direction</param>
	public Ball SpawnBall(Vector2 direction, Vector2 xRange, Vector2 yRange, bool isExtra = false, bool randomColor = false)
	{
		Ball ball = SpawnBall(isExtra);
		
		if (randomColor)
			ball.GetComponent<SpriteRenderer>().color = new Color(Random.value,Random.value,Random.value);
		
		if (direction == Vector2.zero)
			direction = new Vector2(Random.Range(xRange.x, xRange.y), Random.Range(yRange.x, yRange.y));
		
		ball.GetComponent<Rigidbody2D>().AddForce(direction, ForceMode2D.Impulse);
		
		return ball;
	}
}