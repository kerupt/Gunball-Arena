﻿using UnityEngine;
using System.Collections;

public class Wrapping : MonoBehaviour {
	private float topBoundary = 4;
	private float bottomBoundary = -5;
	
	// Update is called once per frame
	void Update(){	
		ScreenWrap();
	}
	
	void ScreenWrap(){	
		Vector3 newPosition = transform.position;
		bool posChanged = false;
		if (transform.position.y > topBoundary){
			newPosition.y = bottomBoundary;
			posChanged = true;
		}

		if (transform.position.y < bottomBoundary){
			newPosition.y = topBoundary;
			posChanged = true;
		}

		if (posChanged){
			transform.position = newPosition;
		}
	}
}
