﻿using UnityEngine;
using System.Collections;

public class CharacterDictionary : MonoBehaviour 
{
	[System.Serializable]
	public class CharacterAssetInformation
	{
		[SerializeField]
		string mCharacterID;
		public string CharacterID { get { return mCharacterID; } }
		[SerializeField]
		GameObject mCharacterPrefab;
		public GameObject CharacterPrefab { get { return mCharacterPrefab; } }
	}

	[SerializeField]
	CharacterAssetInformation[] mCharacters;

	public GameObject this[string ID]
	{
		get { return getCharacterPrefab(ID); }
	}

	GameObject getCharacterPrefab(string characterID)
	{
		GameObject characterPrefab = null;
		for (int i = 0; i < mCharacters.Length; i++)
		{
			if (mCharacters[i].CharacterID.Equals(characterID))
			{
				characterPrefab = mCharacters[i].CharacterPrefab;
				break;
			}
		}

		return characterPrefab;
	}
}
