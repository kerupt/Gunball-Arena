﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerDropMine : PlayerShoot {
	LinkedList<GameObject> mPlantedMines;
	[SerializeField]
	uint mMaxMines = 3;

	protected override void Start()
	{
		mPlantedMines = new LinkedList<GameObject> ();
		base.Start ();
	}

	void FixedUpdate()
	{

	}

	protected override void Fire()
	{
		//Check for any null
		LinkedList<GameObject> removedItems = new LinkedList<GameObject> ();
		foreach (var item in mPlantedMines) {
			if (item == null)
				removedItems.AddLast(item);
		}
		foreach (var item in removedItems) {
			mPlantedMines.Remove(item);
		}

		if (mPlantedMines.Count >= mMaxMines) {
			GameObject mine = mPlantedMines.First.Value;
			mine.SendMessage ("Explode");
			mPlantedMines.RemoveFirst();
		}
		AudioSource.PlayClipAtPoint(mShootSound, transform.position);
		GameObject projectile = (GameObject)Object.Instantiate(mProjectilePrefab, mWeaponMuzzle.transform.position, mPlayer.GetArmPivot().transform.rotation);
		Mine mineScript = projectile.GetComponent<Mine> ();

		mineScript.SetShotFrom (gameObject);

		mPlantedMines.AddLast (projectile);
	}
}
