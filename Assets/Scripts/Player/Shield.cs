﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour 
{
	[SerializeField]
	float mActiveTime = 0.5f;
	[SerializeField]
	float mCooldownTime = 0.5f;
	public float CooldownTime { get { return mCooldownTime; } }
	[SerializeField]
	float mMaxOpacity = 0.5f;
	[SerializeField]
	float mBallSpeedModifier = 1.0f;

	public bool IsActive { get { return mFading; } }

	bool mFading = false;
	float lastUseTime = 0.0f;

	SpriteRenderer mSprite;
	CircleCollider2D mCollider;

	float mFadeDelta = 0.0f;

	bool isSet = false;

	[SerializeField]
	AudioClip ShieldUp;

	void Start()
	{
		Reset();
	}

	void Awake()
	{
		mSprite = GetComponent<SpriteRenderer>();
		mCollider = GetComponent<CircleCollider2D>();
		mCollider.enabled = false;
		lastUseTime = -CooldownTime;
	}

	void Update()
	{
		if (mFading)
		{
			if (mFadeDelta < 1)
			{
				mFadeDelta += (Time.deltaTime/mActiveTime);
			}
			float alpha = Mathf.Lerp(mMaxOpacity, 0.0f, mFadeDelta);
			mSprite.color = new Color(mSprite.color.a, mSprite.color.g, mSprite.color.b, alpha);

			if (Mathf.Approximately(alpha, 0.0f))
			{
				hideShield();
			}
		}
	}

	public void ShowShield()
	{
		if (Time.time > lastUseTime + CooldownTime)
		{
			AudioSource.PlayClipAtPoint(ShieldUp, transform.position);
			gameObject.SetActive(true);
			mCollider.enabled = true;
			mSprite.color = new Color(mSprite.color.a, mSprite.color.g, mSprite.color.b, mMaxOpacity);
			
			mFading = true;		
			mFadeDelta = 0.0f;
			lastUseTime = Time.time;
		}
	}

	void hideShield()
	{
		mCollider.enabled = false;
		mSprite.color = new Color(mSprite.color.a, mSprite.color.g, mSprite.color.b, 0.0f);

		mFading = false;
		gameObject.SetActive(false);
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Ball")
		{
			collision.gameObject.GetComponent<Ball>().AdjustVelocity(-collision.contacts[0].normal, mBallSpeedModifier);
		}
	}


	float mInitialActiveTime;
	float mInitialBallSpeedModifier;
	public void SetShieldModifiers(float durationModifier, float powerModifier)
	{
		if (!isSet) Reset();
		mActiveTime = mInitialActiveTime * durationModifier;
		mBallSpeedModifier = mInitialBallSpeedModifier * powerModifier;
	}

	void Reset()
	{
		mInitialActiveTime = mActiveTime;
		mInitialBallSpeedModifier = mBallSpeedModifier;
		isSet = true;
	}
}
