﻿using UnityEngine;
using System.Collections;

public class PlayerSpriteCooldownHandler : MonoBehaviour 
{
	[System.Serializable]
	class PlayerSpriteComponent
	{
		[SerializeField]
		SpriteRenderer mSprite;
		[SerializeField]
		Color mCooldownColor;
		public Color CooldownColor { get { return mCooldownColor; } }

		public Color mInitSpriteColor { get; private set; }
		public Color mColor { get { return mSprite.color; } set { mSprite.color = value; } }

		public bool mIsFading { get; set; }

		public void InitializeSpriteColor()
		{
			mInitSpriteColor = mSprite.color;
			Debug.Log (mInitSpriteColor);
		}

		public bool IsSet { get { return mSprite != null; } }

	}//PlayerSpriteComponent

	[SerializeField]
	PlayerSpriteComponent mMainSprite;
	[SerializeField]
	PlayerSpriteComponent mJetpackSprite;
	[SerializeField]
	PlayerSpriteComponent mGunSprite;

	void Start () 
	{
		if (mMainSprite.IsSet) { mMainSprite.InitializeSpriteColor(); }
		if (mJetpackSprite.IsSet) { mJetpackSprite.InitializeSpriteColor(); }
		if (mGunSprite.IsSet) { mGunSprite.InitializeSpriteColor(); }
	}

	public void ActivateMainSpriteCooldown(float timeToFadeBack)
	{
		if (mMainSprite.IsSet)
		{
			StartCoroutine(setToTintAndFadeBackCoroutine(mMainSprite, timeToFadeBack));
		}
	}

 	public void ActivateGunSpriteCooldown(float timeToFadeBack)
	{
		if (mGunSprite.IsSet)
		{
			StartCoroutine(setToTintAndFadeBackCoroutine(mGunSprite, timeToFadeBack));
		}
	}

	public void ActivateJetpackSpriteCooldown(float timeToFadeBack)
	{
		if (mGunSprite.IsSet)
		{
			StartCoroutine(setToTintAndFadeBackCoroutine(mJetpackSprite, timeToFadeBack));
		}
	}

	IEnumerator setToTintAndFadeBackCoroutine(PlayerSpriteComponent sprite, float timeToFadeBack)
	{		
		if (!sprite.mIsFading)
		{
			sprite.mColor = sprite.CooldownColor;
			
			float progress = 0.0f;

			sprite.mIsFading = true;
			while(progress < 1.0f)
			{
				sprite.mColor = Color.Lerp(sprite.CooldownColor, sprite.mInitSpriteColor, Mathf.Clamp01(progress));
				progress += Time.deltaTime/timeToFadeBack;

				yield return new WaitForEndOfFrame();
			}
			sprite.mIsFading = false;
		}		
	}

	public Color RandomizeBodyColor()
	{
		return mMainSprite.mColor = new Color(Random.Range(0.0f,1.0f),Random.Range(0.0f,1.0f),Random.Range(0.0f,1.0f));
	}
	public void SetBodyColor(Color color)
	{
		mMainSprite.mColor = color;
	}
	public Color GetBodyColor()
	{
		return mMainSprite.mColor;
	}
}
