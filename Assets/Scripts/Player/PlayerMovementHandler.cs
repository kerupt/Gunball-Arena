﻿using UnityEngine;
using System.Collections;

public class PlayerMovementHandler : MonoBehaviour 
{
	[SerializeField]
	float mMoveSpeed = 1.0f;
	public float MoveSpeed
	{
		get {return mMoveSpeed;}
		set {mMoveSpeed = value;}
	}
	[SerializeField]
	float mDashSpeed = 25.0f;
	[SerializeField]
	float mCooldown = 1.0f;
	float mLastDashUsedTime = -1.0f;

	[SerializeField]
	float mRotAnglePerSecond = 90.0f;
	[SerializeField]
	GameObject mModel;
	[SerializeField]
	GameObject mRocketSmoke;
	[SerializeField]
	protected AudioSource JetpackSound;
	private float audioVolume = 1.0f;

	public bool DashReady
	{
		get
		{
			float timePassed = Time.time - mLastDashUsedTime;
			return (timePassed > mCooldown);
		}
	}

	Rigidbody2D mRigidBody;

	void Awake()
	{
		mRigidBody = GetComponent<Rigidbody2D>();
	}

	void Start () 
	{
		//Make Dash ready from the start
		mLastDashUsedTime = -mCooldown;
	}

	void FadeOut(AudioSource audio) {
		if(audioVolume > 0){
			audioVolume -= 1.0f * Time.deltaTime;
			audio.volume = audioVolume;
		}else{
			audioVolume = 0;
			audio.volume = audioVolume;
			audio.Stop();
		}
	}

	public void HandleBasicMovement(float horizontalAxis, float verticalAxis)
	{
		Vector3 movementVector = new Vector3(horizontalAxis, verticalAxis, 0.0f) * mMoveSpeed;
		mRigidBody.AddForce(movementVector, ForceMode2D.Impulse);
		faceDirection(movementVector);
		mRocketSmoke.SetActive(true);
		audioVolume = 1.0f;
		JetpackSound.volume = audioVolume;

		if (!JetpackSound.isPlaying){
			JetpackSound.loop = true;
			JetpackSound.Play();
		}
	}

	public void HandleNoMovement(float horizontalAxis, float verticalAxis)
	{
		Vector3 movementVector = new Vector3(horizontalAxis, verticalAxis, 0.0f);
		faceDirection(movementVector);
		mRocketSmoke.SetActive(false);
		JetpackSound.loop = false;
		FadeOut (JetpackSound);

	}

	public void HandleDashMovement(float horizontalAxis, float verticalAxis)
	{
		if (DashReady)
		{
			Vector3 movementVector = new Vector3(horizontalAxis, verticalAxis, 0.0f) * mDashSpeed;

			mRigidBody.AddForce(movementVector, ForceMode2D.Impulse);
			mLastDashUsedTime = Time.time;

			faceDirection(movementVector);

			if(GetComponent<PlayerSpriteCooldownHandler>())
			{
				GetComponent<PlayerSpriteCooldownHandler>().ActivateJetpackSpriteCooldown(mCooldown);
			}
		}
	}

	void Update()
	{
		mModel.transform.rotation = Quaternion.RotateTowards(mModel.transform.rotation, Quaternion.AngleAxis(0.0f, Vector3.forward), mRotAnglePerSecond * Time.deltaTime);
	}

	void faceDirection(Vector2 movementVector)
	{
		if (movementVector.magnitude > 0)
		{
			float angle = Mathf.Atan2(movementVector.y, movementVector.x) * Mathf.Rad2Deg;
			Quaternion q = Quaternion.AngleAxis(angle - 90.0f, Vector3.forward);
			mModel.transform.rotation = q;
		}

		//Face Right
		if(mModel.transform.rotation.eulerAngles.z % 360.0f >= 180.0f)
		{
			Vector3 newScale = new Vector3(Mathf.Abs (mModel.transform.localScale.x), mModel.transform.localScale.y, mModel.transform.localScale.z);
			mModel.transform.localScale = newScale;
			mRocketSmoke.transform.localRotation = Quaternion.Euler(Vector3.zero);
		}
		else if (mModel.transform.rotation.eulerAngles.z == 0.0f)
		{
			//Stay Facing whatever direction;
		}
		//Face Left
		else
		{
			Vector3 newScale = new Vector3(-Mathf.Abs (mModel.transform.localScale.x), mModel.transform.localScale.y, mModel.transform.localScale.z);
			mModel.transform.localScale = newScale;
			mRocketSmoke.transform.localRotation = Quaternion.Euler(45.0f * Vector3.forward);
		}
	}
}
