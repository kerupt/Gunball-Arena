﻿using UnityEngine;
using System.Collections;

public class PlayerShootCharged : PlayerShoot {
	[SerializeField]
	float mMaxChargedSpeed = 10;
	[SerializeField]
	float mChargeSpeed = 0.5f;

	bool isChargeDown = false;
	bool isNormalDown = false;
	void FixedUpdate()
	{
		isNormalDown = Input.GetButton ("FireP" + mPlayer.GetPlayerNumber ());
		isChargeDown = (Input.GetAxisRaw ("Fire2P" + mPlayer.GetPlayerNumber ()) > 0.15f);
	}

	protected override IEnumerator HandleShoot()
	{
		float charge;
		bool shot = false;
		float defaultSpeed = mProjectilePrefab.GetComponent<BaseProjectile> ().MoveSpeed;
		while(true)
		{
			if (isChargeDown)
			{
				charge = defaultSpeed;
				while (isChargeDown || !shot)
				{
					charge += mChargeSpeed * Time.deltaTime;
					yield return null;
					if (charge > mMaxChargedSpeed)
					{
						charge = mMaxChargedSpeed;
//						isChargeDown = false;
					}
					if (!isChargeDown)
					{
						shot = true;
						Fire (charge);
						yield return new WaitForSeconds(mBurstDelay);
						break;
					}
				}
			}
			else if (isNormalDown)
			{
				Fire ();
				yield return new WaitForSeconds(mBurstDelay);
			}
			yield return null;
		}
	}

	protected void Fire(float chargedSpeed)
	{
		AudioSource.PlayClipAtPoint(mShootSound, transform.position);
		GameObject projectile = (GameObject)Object.Instantiate(mProjectilePrefab, mWeaponMuzzle.transform.position, mPlayer.GetArmPivot().transform.rotation);
		projectile.GetComponent<BaseProjectile> ().SetShotFrom (gameObject);
		projectile.GetComponent<BaseProjectile> ().MoveSpeed = chargedSpeed;
	}
}
