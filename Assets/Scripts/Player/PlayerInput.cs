﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {
	[SerializeField]
	int mPlayerNumber = 1;
	public int GetPlayerNumber(){return mPlayerNumber;}
	public void SetPlayerNumber(int value)
	{
		mPlayerNumber = value;
	}

	public float mSizeModifier = 1;
	
	[SerializeField]
	GameObject mArmPivot;
	public GameObject GetArmPivot(){ return mArmPivot; }
	[SerializeField]
	float mPivotSpeed = 50;

	//Movement
	[SerializeField]
	float mDoubleTapSpeed = 0.18f;
	int mMoveTapCheck = 0;
	float mLastTapTime = 0.0f;
	float mMoveHoldCounter = 0.0f;

	//Blocking
	[SerializeField]
	Shield mShield;


	public bool ShieldIsActive { get { return mShield.IsActive; } }

	PlayerMovementHandler mMovementController;

	[SerializeField]
	Collider2D mMainCollider;
	[SerializeField]
	Collider2D mStunCollider;

	void Awake()
	{
		mMovementController = GetComponent<PlayerMovementHandler>();
	}

	void Update()
	{
		HandleAiming ();

		HandleMovementInput();

		HandleBlockInput();
	}

	void HandleBlockInput()
	{
		if ((Input.GetAxisRaw("BlockP" + mPlayerNumber) > 0.15f) && !mShield.IsActive)
		{
			mShield.ShowShield();

			if(GetComponent<PlayerSpriteCooldownHandler>())
			{
				GetComponent<PlayerSpriteCooldownHandler>().ActivateMainSpriteCooldown(mShield.CooldownTime);
			}
		}
	}

	void HandleAiming()
	{
		float aim = Input.GetAxisRaw("AimP" + mPlayerNumber);
		mArmPivot.transform.Rotate(new Vector3(0,0,1), Time.deltaTime * mPivotSpeed * aim);

		float joyH = Input.GetAxisRaw ("AimJoyHP" + mPlayerNumber);
		float joyV = Input.GetAxisRaw ("AimJoyVP" + mPlayerNumber);

		if (!Mathf.Approximately (joyH, 0.0f) ||
			!Mathf.Approximately (joyV, 0.0f)) {
//			Mathf.Lerp(
//				mArmPivot.transform.rotation,
//				Quaternion.Euler(
//					0.0f,
//					0.0f,
//					Mathf.Atan2(joyH, joyV) * Mathf.Rad2Deg
//				)	
//				,Time.time
//			);
			mArmPivot.transform.rotation = 
				Quaternion.Euler(
					0.0f,
					0.0f,
					Mathf.Atan2(joyH, joyV) * Mathf.Rad2Deg
				);
		}
	}

	void HandleMovementInput()
	{
		float horizontal = Input.GetAxisRaw("HorizontalP" + mPlayerNumber);
		float vertical = Input.GetAxisRaw("VerticalP" + mPlayerNumber);

		if (new Vector2(horizontal, vertical).magnitude < 0.25f)
		{
			horizontal = 0.0f;
			vertical = 0.0f;
		}

		if (Input.GetAxisRaw("AccelerateP" + mPlayerNumber) > 0.15f)
		{
			mMovementController.HandleDashMovement(horizontal,vertical);
		}

		if (new Vector2(horizontal, vertical).magnitude > 0.0f)
		{
			mMovementController.HandleBasicMovement(horizontal,vertical);
		}
		else
		{
			mMovementController.HandleNoMovement(horizontal,vertical);
		}
	}//HandleMovementInput
}
