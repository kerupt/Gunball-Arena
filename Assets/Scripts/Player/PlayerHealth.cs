﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour 
{

	PlayerInput mInputScript;
	Rigidbody2D body;
	void Start()
	{
		mcurrentHP = mMaxHealth;
		mInputScript = GetComponent<PlayerInput> ();
		body = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
		if (body.angularVelocity > 0.0f)
			body.angularVelocity -= Time.deltaTime;
	}


//	public void AddForce(Vector2 direction)
//	{
//		transform.GetComponent<Rigidbody2D> ().AddForce (direction, ForceMode2D.Impulse);
//	}

	//TODO Change location [PlayerHealth script or something]
	[SerializeField]
	float mMaxHealth = 50;
	float mcurrentHP;

	[SerializeField]
	AudioClip GetHitSound1;
	[SerializeField]
	AudioClip GetHitSound2;

	[SerializeField]
	Animator stunAnimation;
	[SerializeField]
	AudioClip stunSound;

	/// <summary>
	/// The player takes damage
	/// </summary>
	/// <param name="damage">Damage taken</param>
	public void TakeDamage(float damage)
	{
		AudioClip randomSound = null;
		float SoundIndex = Random.Range(1,3); // returns 1 or 2
		if (SoundIndex == 1){
			randomSound = GetHitSound1;
		}else if (SoundIndex == 2){
			randomSound = GetHitSound2;
		}
		AudioSource.PlayClipAtPoint(randomSound, transform.position);
		if (isStunned || mInputScript.ShieldIsActive)
			return;
		mcurrentHP -= damage;
		if (mcurrentHP < 0) {
			//TODO Disable player input script for a period of time
			StartCoroutine("Stunned");

//			GetComponent<BoxCollider2D>().enabled = false;
//			GetComponent<PolygonCollider2D>().enabled = true;

			mcurrentHP = mMaxHealth;
		}
	}

	[SerializeField]
	float mStunnedTime = 0.5f;
	bool isStunned = false;
	IEnumerator Stunned()
	{
		//TODO Play stun animation [ex: stars]

		isStunned = true;

		//Keep previous rigidbody values
		Rigidbody2D body = GetComponent<Rigidbody2D> ();
		float initialMass = body.mass;
		float initialAngularDrag = body.angularDrag;

		//Change to stunned values
		body.mass = 10;
		body.angularDrag = 0.05f;

		DisableScripts ();
		stunAnimation.SetBool("Stun", true);
		PlayClipDestroyedObject.PlayClipAtPoint(stunSound, transform.position, mStunnedTime);

		//Stun for X
		yield return new WaitForSeconds (mStunnedTime);

		EnableScripts ();
		stunAnimation.SetBool("Stun", false);

		//Reset body values
		body.mass = initialMass;
		body.angularDrag = initialAngularDrag;

		isStunned = false;
	}

	void DisableScripts()
	{
		SetScriptsState (false);
	}

	void EnableScripts()
	{
		SetScriptsState (true);
	}

	void SetScriptsState(bool enabled)
	{
		//Get scripts
		PlayerShoot shootScript = GetComponent<PlayerShoot> ();
		
		//Disable
		if (mInputScript != null)
			mInputScript.enabled = enabled;
		if (shootScript != null)
			shootScript.enabled = enabled;
	}
}
