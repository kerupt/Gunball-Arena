﻿using UnityEngine;
using System.Collections;

public class PlayerBlockHandler : MonoBehaviour 
{
	[SerializeField]
	Shield shield;

	public Shield GetShield() {
		return shield;
	}

	public void EnableShield()
	{
		shield.gameObject.SetActive(true);
	}
}
