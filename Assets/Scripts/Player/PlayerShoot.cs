﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {
	[SerializeField]
	protected float mBurstSize = 1;

	protected PlayerInput mPlayer;

	[SerializeField]
	protected float mBurstShootSpeed = 0.5f;
	public float BurstShootSpeed
	{
		get { return mBurstShootSpeed;}
		set { mBurstShootSpeed = value;}
	}
	[SerializeField]
	protected float mBurstDelay = 0.5f;
	public float BurstDelay
	{
		get { return mBurstDelay;}
		set { mBurstDelay = value;}
	}


	[SerializeField]
	protected AudioClip mShootSound;
	[SerializeField]
	protected GameObject mProjectilePrefab;

	protected virtual void Start()
	{
		mPlayer = gameObject.GetComponent<PlayerInput> ();
		if (mPlayer == null) {
			throw new UnityException("Player input script required on player");
			return;
		}
		StartCoroutine ("HandleShoot");
	}

	protected virtual IEnumerator HandleShoot()
	{
		while(true)
		{
			if(Input.GetButton ("FireP" + mPlayer.GetPlayerNumber()))
			{
				for (int i = 0; i < mBurstSize; ++i)
				{
					//Case where the player lets go in a long burst
					if (!Input.GetButton ("FireP" + mPlayer.GetPlayerNumber()))
						break;

					Fire ();
					yield return new WaitForSeconds(mBurstShootSpeed);
				}
				yield return new WaitForSeconds(mBurstDelay);
			}
			yield return null;
		}
	}

	[SerializeField]
	protected GameObject mWeaponMuzzle;
	protected virtual void Fire()
	{

		AudioSource.PlayClipAtPoint(mShootSound, transform.position);
		//GameObject projectile = (GameObject)Object.Instantiate(mProjectilePrefab, mPlayer.GetArmPivot().transform.GetChild(0).position, mPlayer.GetArmPivot().transform.rotation);

		GameObject projectile = (GameObject)Object.Instantiate(mProjectilePrefab, mWeaponMuzzle.transform.position, mPlayer.GetArmPivot().transform.rotation);

		projectile.GetComponent<BaseProjectile> ().SetShotFrom (gameObject);
		if (GetComponent<PlayerSpriteCooldownHandler>() != null)
		{
			GetComponent<PlayerSpriteCooldownHandler>().ActivateGunSpriteCooldown(mBurstDelay);
		}
	}

	protected virtual void OnEnable()
	{
		if (mPlayer == null)
			return;
		StartCoroutine ("HandleShoot");
	}

	protected virtual void OnDisable()
	{
		StopCoroutine("HandleShoot");
	}
}
