﻿using UnityEngine;
using System.Collections;

public class GameStarter : MonoBehaviour 
{
	[SerializeField]
	bool mStartOnStart = true;
	// Use this for initialization
	void Start () 
	{
		if (mStartOnStart)
		{
			CallStartMatch();
		}
	}

	public void CallStartMatch()
	{
		GameManager.manager.StartMatch();
	}
}
