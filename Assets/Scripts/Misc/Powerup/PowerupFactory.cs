﻿using UnityEngine;
using System.Collections;

namespace Misc.Powerup
{
	public class PowerupFactory : MonoBehaviour {
		[SerializeField]
		//GameObject mSpawnRange;
		Bounds mSpawnRange;

		[SerializeField]
		GameObject[] mPowerupPrefabs;
		[SerializeField]
		float[] mDropTable;

		[SerializeField]
		bool mHelpLosingPlayer = true;

		[SerializeField]
		float mSpawnTimer = 5;

		[SerializeField]
		AudioClip dropPowerUp;

		void Start()
		{
			if (mPowerupPrefabs.Length == 0)
				Debug.Log ("Add powerups to the powerup factory to spawn them");
			else
			{
				PrepareDropTable();

				StartCoroutine("ConstantPower");
			}
		}

		void PrepareDropTable()
		{
			mDropTable = new float[mPowerupPrefabs.Length];
			float total = 0;
			for (int i = 0; i < mPowerupPrefabs.Length; ++i) {
				int rare = mPowerupPrefabs[i].GetComponent<VisualPowerup>().GetRarity();
				total += mDropTable[i] = rare;
			}
			for (int i = 0; i < mPowerupPrefabs.Length; ++i) {
				mDropTable[i] /= total;
			}
		}

		IEnumerator ConstantPower()
		{
			while (true) {
				yield return new WaitForSeconds (mSpawnTimer);
				InstantiatePowerup ();
			}
		}

		void InstantiatePowerup()
		{
			//TODO To make it a bit more fun for the losing player,
			//get the score and spawn a better powerup in his area

			//Check if the position is colliding with another object
			//Change position if so
			GameObject powerUp = GetRandomPowerup ();
			CircleCollider2D col = powerUp.GetComponent<CircleCollider2D> ();
			Vector3 position;
			do {
				position = PositionPowerup ();
			} while (col != null && 
			         Physics2D.CircleCast(
						position,
						col.radius,
						Vector2.zero,
						1,
						GetCollisionLayerMask()
				)
			);


			Instantiate (powerUp,
			            position,
			            Quaternion.identity);

			AudioSource.PlayClipAtPoint(dropPowerUp, transform.position);
		}

		[SerializeField]
		string[] SpawnHitLayers = new string[]{"Bumpers"};
		int GetCollisionLayerMask()
		{
			//Generate the hit from strings
			int hit = 0;
			int mask = 0;
			foreach (var layer in SpawnHitLayers) {
				mask = LayerMask.NameToLayer(layer);
				if (mask < 0) 
				{
					Debug.Log (string.Format("<color=red>{0}</color> is not a valid layer",layer));
					continue;
				}
				hit |= 1 << mask;
			}

			return hit;
		}

		/// <summary>
		/// Currently random.
		/// </summary>
		/// <returns>The random powerup.</returns>
		GameObject GetRandomPowerup()
		{
			float t = Random.Range (0.0f, 1f);
			float total = 0;
			int i = 0;
			for(;i < mPowerupPrefabs.Length - 1; ++i)
			{
				total += mDropTable[i];
				if (t <= total)
					break;
			}
			return mPowerupPrefabs [i];
		}

		Vector3 PositionPowerup ()
		{
			//TODO Create spawn zone, add greater chance of 
			//spawning near losing player
			float min;
			float max;
			GetMinMaxXPosition(out min, out max);

			Vector3 pos = new Vector3(
				Random.Range(min, max),
				Random.Range(mSpawnRange.min.y, mSpawnRange.max.y),
				0);
			return pos;
		}

		public void GetMinMaxXPosition(out float min, out float max)
		{
			if (mHelpLosingPlayer) {
				//+ P1 in lead
				//- P2 in lead
				int diff = GameManager.manager.GetPlayerScore(1) - GameManager.manager.GetPlayerScore(2);
				min = -3;
				max = 3;
				if (diff > 0)
					max += diff;
				else 
					min += diff;
				float fieldLocation = Random.Range (min, max);
				//Debug.Log (fieldLocation);
				min = fieldLocation <= 0 ? mSpawnRange.min.x : mSpawnRange.min.x / 10;
				max = fieldLocation >= 0 ? mSpawnRange.max.x : mSpawnRange.max.x / 10;
			} else {
				min = mSpawnRange.min.x;
				max = mSpawnRange.max.x;
			}
		}
	}
}