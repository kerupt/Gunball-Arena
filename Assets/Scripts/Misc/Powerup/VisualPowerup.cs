﻿using UnityEngine;
using System.Collections;
using Misc.Powerup;

[RequireComponent(typeof(Collider2D))]
public class VisualPowerup: MonoBehaviour {
	[SerializeField]
	int mRarity;

	[SerializeField]
	PowerupType mPowerupType;

	[SerializeField]
	AudioClip pickUpPowerup;

	public int GetRarity(){ return mRarity; }

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.attachedRigidbody.CompareTag("Player")) 
		{
			PlayClipDestroyedObject.PlayClipAtPoint(pickUpPowerup, transform.position);
			GameObject o = other.attachedRigidbody.gameObject;
			o.SendMessage("ApplyPowerup", mPowerupType);
			Destroy(gameObject);
		}
	}
}
