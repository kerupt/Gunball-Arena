﻿using UnityEngine;
using System.Collections;
using Misc.Powerup;

public enum PowerupType {
	BiggerCharacter, BiggerOpponentNet, FasterMovement,
	FasterShooting, SmallerNet, StrongerShield, Super
}