﻿using UnityEngine;
using System.Collections;

public class SmallerNet : NetSizeChanger {
	protected override Goal GetPlayerGoal(int num)
	{
		switch (num) {
		case 1:
			return GameManager.manager.CurrentlyLoadedStage.Team1Goal; 
		case 2:
			return GameManager.manager.CurrentlyLoadedStage.Team2Goal; 
		default:
			return null;
		}
	}
}