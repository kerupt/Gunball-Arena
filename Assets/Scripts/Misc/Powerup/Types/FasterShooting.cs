﻿using UnityEngine;
using System.Collections;

public class FasterShooting : BasePowerup {
	[SerializeField]
	float mBurstShootSpeedModifier = 2f;
	[SerializeField]
	float mBurstDelayModifier = 2f;

	public override void Execute ()
	{
		StartCoroutine(COROUTINE_NAME);
	}
	
	public override void Reapply ()
	{
		StopCoroutine(COROUTINE_NAME);
		StartCoroutine(COROUTINE_NAME);
		base.Reapply ();
	}
	
	IEnumerator ExecutePowerup()
	{
		mPlayer.GetComponent<PlayerPowerupModifier>().SetShootSpeed(mBurstShootSpeedModifier, mBurstDelayModifier);
		yield return new WaitForSeconds (mDuration);
		Destroy(gameObject);
	}
	void OnDestroy()
	{
		mPlayer.GetComponent<PlayerPowerupModifier> ().ResetShootSpeed ();
		if (ExpiredEvent != null)
			ExpiredEvent (this);
	}
}