﻿using UnityEngine;
using System.Collections;

public class BiggerCharacter : BasePowerup {
	[SerializeField]
	float mSizeModifier = 2;

	public override void Execute ()
	{
		StartCoroutine(COROUTINE_NAME);
	}
	
	public override void Reapply ()
	{
		StopCoroutine(COROUTINE_NAME);
		StartCoroutine(COROUTINE_NAME);
		base.Reapply ();
	}
	
	IEnumerator ExecutePowerup()
	{
		mPlayer.GetComponent<PlayerPowerupModifier>().SetScale(mSizeModifier);
		yield return new WaitForSeconds (mDuration);
		Destroy(gameObject);
	}
	void OnDestroy()
	{
		mPlayer.GetComponent<PlayerPowerupModifier>().ResetScale();
		if (ExpiredEvent != null)
			ExpiredEvent (this);
	}
}
