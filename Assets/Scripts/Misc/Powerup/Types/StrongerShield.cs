﻿using UnityEngine;
using System.Collections;

public class StrongerShield : BasePowerup {
	[SerializeField]
	float mDurationModifier = 1f;
	[SerializeField]
	float mPowerModifier = 1.5f;
	
	public override void Execute ()
	{
		StartCoroutine(COROUTINE_NAME);
	}
	
	public override void Reapply ()
	{
		StopCoroutine(COROUTINE_NAME);
		StartCoroutine(COROUTINE_NAME);
		base.Reapply ();
	}
	
	IEnumerator ExecutePowerup()
	{
		mPlayer.GetComponent<PlayerPowerupModifier>().SetShieldModifiers(mDurationModifier, mPowerModifier);
		yield return new WaitForSeconds (mDuration);
		Destroy(gameObject);
	}
	void OnDestroy()
	{
		mPlayer.GetComponent<PlayerPowerupModifier> ().ResetShieldModifiers ();
		if (ExpiredEvent != null)
			ExpiredEvent (this);
	}
}