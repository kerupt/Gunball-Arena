﻿using UnityEngine;
using System.Collections;

public class BiggerOpponentNet : NetSizeChanger {
	protected override Goal GetPlayerGoal(int num)
	{
		switch (num) {
		case 1:
			return GameManager.manager.CurrentlyLoadedStage.Team2Goal; 
		case 2:
			return GameManager.manager.CurrentlyLoadedStage.Team1Goal; 
		default:
			return null;
		}
	}
}