﻿using UnityEngine;
using System.Collections;
using System;

public class Super : BasePowerup {
	[SerializeField]
	PowerupType[] mTypes;

	public override void Execute ()
	{
		StartCoroutine(COROUTINE_NAME);
	}
	
	public override void Reapply ()
	{
		StopCoroutine(COROUTINE_NAME);
		StartCoroutine(COROUTINE_NAME);
		base.Reapply ();
	}
	
	IEnumerator ExecutePowerup()
	{
		foreach (var type in mTypes) {
			//No infinite supers here
			if (type.GetType().Equals(this.GetType()))
				continue;
			mPlayer.SendMessage("ApplyPowerup", type);
		}
		yield return new WaitForSeconds (mDuration);
		Destroy(gameObject);
	}
	void OnDestroy()
	{
		if (ExpiredEvent != null)
			ExpiredEvent (this);
	}
}