﻿using UnityEngine;
using System.Collections;

public class FasterMovement : BasePowerup {
	[SerializeField]
	float mSpeedModifier = 2f;

	public override void Execute ()
	{
		StartCoroutine(COROUTINE_NAME);
	}
	
	public override void Reapply ()
	{
		StopCoroutine(COROUTINE_NAME);
		StartCoroutine(COROUTINE_NAME);
		base.Reapply ();
	}
	
	IEnumerator ExecutePowerup()
	{
		mPlayer.GetComponent<PlayerPowerupModifier>().SetMovementSpeed(mSpeedModifier);
		yield return new WaitForSeconds (mDuration);
		Destroy(gameObject);
	}
	void OnDestroy()
	{
		mPlayer.GetComponent<PlayerPowerupModifier>().ResetMovement();
		if (ExpiredEvent != null)
			ExpiredEvent (this);
	}
}