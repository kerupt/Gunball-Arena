﻿using UnityEngine;
using System.Collections;

public abstract class NetSizeChanger : BasePowerup {
	[SerializeField]
	protected float mSizeModifier = 0.2f;
	
	protected GoalSizeHandler mGoalSizeHandlerScript;
	
	public override void Execute ()
	{
		InitializeGoalScript ();
		StartCoroutine(COROUTINE_NAME);
	}
	
	protected void InitializeGoalScript()
	{
		PlayerInput pi = mPlayer.GetComponent<PlayerInput> ();
		if (pi == null)
			throw new UnityException("Player input script required on players");
		
		Goal goal = GetPlayerGoal(pi.GetPlayerNumber());
		
		if (goal == null)
			throw new UnityException("Unexpected player number");
		
		mGoalSizeHandlerScript = goal.transform.parent.GetComponent<GoalSizeHandler> ();
		
		if (mGoalSizeHandlerScript == null)
			throw new UnityException("Goals require the Goal size handler script");
	}
	
	protected abstract Goal GetPlayerGoal (int num);

	public override void Reapply ()
	{
		mGoalSizeHandlerScript.RemoveSizeModifier (mSizeModifier);
		StopCoroutine(COROUTINE_NAME);
		StartCoroutine(COROUTINE_NAME);
		base.Reapply ();
	}
	
	IEnumerator ExecutePowerup()
	{
		mGoalSizeHandlerScript.AddSizeModifier (mSizeModifier);
		yield return new WaitForSeconds (mDuration);
		Destroy(gameObject);
	}
	void OnDestroy()
	{
		mGoalSizeHandlerScript.RemoveSizeModifier (mSizeModifier);
		if (ExpiredEvent != null)
			ExpiredEvent (this);
	}
}