﻿using UnityEngine;
using System.Collections;
using Misc.Powerup;

public abstract class BasePowerup: MonoBehaviour {
	[SerializeField]
	protected float mDuration = 5f;
	protected GameObject mPlayer;

	public abstract void Execute();
	public virtual void Reapply(){}
	public delegate void ExpiredEventHandler(BasePowerup sender);
	public ExpiredEventHandler ExpiredEvent;

	protected const string COROUTINE_NAME = "ExecutePowerup";
	
	bool onPlayer = false;
	public void Start()
	{
		mPlayer = transform.parent.gameObject;
		if (mPlayer.tag == "Player") {
			onPlayer = true;
			Execute ();
		} else
			mPlayer = null;
	}
}
