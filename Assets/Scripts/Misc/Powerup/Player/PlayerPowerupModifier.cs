﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;


public class PlayerPowerupModifier : MonoBehaviour {
	PlayerInput mPlayerInputScript;
	Vector3 mInitialPlayerScale;

	PlayerShoot mPlayerShootScript;
	Vector2 mInitialPlayerShootSpeed;

	PlayerMovementHandler mPlayerMovementScript;
	float mInitialMovementSpeed;

	Shield mShieldScript;
	float mInitialShieldDuration;
	float mInitialShieldPower;

	void Start()
	{
		mPlayerInputScript = GetComponent<PlayerInput> ();
		mInitialPlayerScale = transform.localScale;

		mPlayerShootScript = GetComponent<PlayerShoot> ();
		if (mPlayerShootScript != null)
			mInitialPlayerShootSpeed = new Vector2 (mPlayerShootScript.BurstDelay, mPlayerShootScript.BurstShootSpeed);

		mPlayerMovementScript = GetComponent<PlayerMovementHandler> ();
		if (mPlayerMovementScript != null)
			mInitialMovementSpeed = mPlayerMovementScript.MoveSpeed;

		PlayerBlockHandler pbh = GetComponent<PlayerBlockHandler> ();
		if (pbh != null) {
			mShieldScript = pbh.GetShield();
//			mini
		}
	}

	public void SetScale(float sizeModifier)
	{
		if (mPlayerInputScript == null)
			throw new Exception ("Player must have a PlayerInput script component");
		mPlayerInputScript.mSizeModifier = sizeModifier;
		transform.localScale = (sizeModifier * mInitialPlayerScale);
	}
	public void ResetScale() { SetScale (1); }

	public void SetShootSpeed(float burstShootSpeedModifier, float burstDelayModifier)
	{
		if (mPlayerShootScript == null)
			throw new Exception ("Player must have a PlayerShoot script component");
		if (burstShootSpeedModifier != 0)
			mPlayerShootScript.BurstShootSpeed = mInitialPlayerShootSpeed[1] / burstShootSpeedModifier;
		if (burstDelayModifier != 0)
			mPlayerShootScript.BurstDelay = mInitialPlayerShootSpeed[0] / burstDelayModifier;
	}
	public void ResetShootSpeed() {	SetShootSpeed (1, 1);}

	public void SetMovementSpeed(float mMovementSpeed)
	{
		if (mPlayerMovementScript == null)
			throw new Exception ("Player must have a PlayerMovementHandler script component");
		mPlayerMovementScript.MoveSpeed = mInitialMovementSpeed * mMovementSpeed;
	}
	public void ResetMovement() {	SetMovementSpeed(1);}

	public void SetShieldModifiers(float durationModifier, float powerModifier)
	{
		mShieldScript.SetShieldModifiers (durationModifier, powerModifier);
	}

	public void ResetShieldModifiers()
	{
		SetShieldModifiers (1, 1);
	}
}
