﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

[Serializable]
public class PowerupObject: IEnumerable {
	public PowerupType Type;
	public GameObject Powerup;

	#region IEnumerable implementation
	
	public IEnumerator GetEnumerator ()
	{
		return (IEnumerator)GetEnumerator ();
	}
	
	#endregion
}


public class PlayerPowerupFactory : MonoBehaviour {
	public PowerupObject[] mPowerups;
	Dictionary<PowerupType, GameObject> mPowers;
	void Start() {
		if (gameObject.tag != "Player")
			Destroy(this);

		mPowers = new Dictionary<PowerupType, GameObject> ();
		foreach (PowerupObject p in mPowerups) {
			mPowers.Add(p.Type, p.Powerup);
		}
	}

	void ApplyPowerup(PowerupType pType)
	{
		if (!mPowers.ContainsKey (pType))
			return;
		GameObject go = mPowers [pType];
		BasePowerup p = go.GetComponent<BasePowerup> ();
		if (p == null)
			return;

		string type = p.GetType().ToString();
		BasePowerup pow = (BasePowerup)gameObject.GetComponentInChildren(Type.GetType(type));
		if (pow != null){
			pow.Reapply();
		}
		else
		{
			GameObject power = Instantiate(go, transform.position, transform.rotation) as GameObject;
			power.transform.parent = transform;
//			AddAndCopyComponent(
//				gameObject.AddComponent(System.Type.GetType(type)) as BasePowerup,
//				p);
		}
	}

	//http://answers.unity3d.com/questions/530178/how-to-get-a-component-from-an-object-and-add-it-t.html
	void AddAndCopyComponent(BasePowerup comp, BasePowerup other)
	{
		Type type = comp.GetType();
		if (type != other.GetType()) return; // type mis-match
		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties(flags);
		foreach (var pinfo in pinfos) {
			if (pinfo.CanWrite) {
				try {
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
			}
		}
		FieldInfo[] finfos = type.GetFields(flags);
		foreach (var finfo in finfos) {
			finfo.SetValue(comp, finfo.GetValue(other));
		}
	}
}
