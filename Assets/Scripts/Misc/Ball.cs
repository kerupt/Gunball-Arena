﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	[SerializeField]
	float mSpeed = 20;
	[SerializeField]
	float mTerminalSpeed = 10;
	Vector2 mCurrentSpeed;

	[SerializeField]
	AudioClip WallHit;

	void Start(){
		if (GameManager.manager.wrapping == 1){
			gameObject.GetComponent<Wrapping>().enabled = true;
		}else {
			gameObject.GetComponent<Wrapping>().enabled = false;
		}
	}

	void Update () {
		mCurrentSpeed = GetComponent<Rigidbody2D> ().velocity;
	}
	
	public void AddForce(Vector2 direction)
	{
		transform.GetComponent<Rigidbody2D> ().AddForce (direction, ForceMode2D.Impulse);
	}

	[SerializeField]
	float mCollisionVelocityModifier = 0.2f;
	void OnCollisionEnter2D(Collision2D other)
	{
		AudioSource.PlayClipAtPoint(WallHit, transform.position);
		if (other.gameObject.layer == LayerMask.NameToLayer("StageBoundary"))
		{
			GetComponent<Rigidbody2D> ().velocity *= .9f;
			GetComponent<LastPlayerHitBy>().OtherHit();
			Debug.Log ("HITTING WALL");

		}
		if (other.gameObject.tag == "Player")
		{
////			Debug.Log(GetComponent<Rigidbody2D> ().velocity);
			Vector2 vel = GetComponent<Rigidbody2D> ().velocity;
////			Vector2 otherVel = other.relativeVelocity;
//			Vector2 otherVel = other.gameObject.GetComponent<Rigidbody2D>().velocity;
//
//			vel = (vel.sqrMagnitude > otherVel.sqrMagnitude ? vel : -otherVel);
			float currentMag = mCurrentSpeed.sqrMagnitude;
			vel = currentMag > vel.sqrMagnitude ? 
				(Mathf.Sqrt(currentMag) * other.contacts[0].normal)
				 :vel;

//			GetComponent<Rigidbody2D> ().velocity = vel * mCollisionVelocityModifier;
			
			GetComponent<Rigidbody2D> ().velocity = vel * mCollisionVelocityModifier;
			//			GetComponent<Rigidbody2D> ().velocity = (-1 * mCurrentSpeed);
//
			other.gameObject.GetComponent<PlayerHealth>().TakeDamage(GetComponent<Rigidbody2D> ().velocity.sqrMagnitude);
			GetComponent<LastPlayerHitBy>().PlayerHit(other.gameObject.GetComponent<PlayerInput>().GetPlayerNumber());
		}
	}

	public void AdjustVelocity(Vector2 newDirection, float speedModifier)
	{
		GetComponent<Rigidbody2D> ().velocity = mCurrentSpeed.magnitude * speedModifier * newDirection;
	}
}
