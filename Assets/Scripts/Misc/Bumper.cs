﻿using UnityEngine;
using System.Collections;

public class Bumper : MonoBehaviour 
{
	[SerializeField]
	float bumperForce = 10.0f;
	[SerializeField]
	float scaleMultiplier = 1.2f;
	[SerializeField]
	float scaleTime = 0.3f;
	[SerializeField]
	GameObject model;

	bool scaling = false;
	Vector3 initialScale;

	void Start()
	{
		initialScale = (model != null) ? model.transform.localScale : Vector3.one;
	}

	protected void OnCollisionEnter2D(Collision2D collision)
	{
		if (!collision.gameObject.CompareTag("Ball"))
		{
			Vector2 dir = -collision.contacts[0].normal.normalized;
			
			collision.collider.attachedRigidbody.AddForce (dir.normalized * bumperForce, ForceMode2D.Impulse);
		}

		if (model != null && scaleMultiplier != 1.0f)
		{
			StartCoroutine(scaleModel());
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		GameObject obj = (collider.attachedRigidbody != null)? collider.attachedRigidbody.gameObject : collider.gameObject;

		if (obj.layer == LayerMask.NameToLayer("Projectile"))
		{
			Destroy (obj);
		}
	}

	IEnumerator scaleModel()
	{
		if (!scaling)
		{
			scaling = true;

			float progress = 0.0f;
			while (model.transform.localScale != initialScale * scaleMultiplier)
			{
				progress += Time.deltaTime/scaleTime;
				model.transform.localScale = Vector3.Lerp(initialScale, initialScale * scaleMultiplier, Mathf.Clamp01(progress));
				yield return new WaitForEndOfFrame();
			}
			
			progress = 0.0f;
			while (model.transform.localScale != initialScale)
			{
				progress += Time.deltaTime/scaleTime;
				model.transform.localScale = Vector3.Lerp(initialScale * scaleMultiplier, initialScale, Mathf.Clamp01(progress));
				yield return new WaitForEndOfFrame();
			}
			scaling = false;
		}
	}
}
