﻿using UnityEngine;
using System.Collections;

public class FlashScreenEvent : BaseRandomEvent 
{
	[SerializeField]
	int flashLoops = 1;
	[SerializeField]
	float flashDuration = 3.0f;

	public override void Execute()
	{
		if (HudManager.Instance != null)
		{
			HudManager.Instance.FlashScreen.Flash(flashLoops,flashDuration);
		}
		else
		{
			Debug.Log ("HudManager is missing!!");
		}
	}
}
