﻿using UnityEngine;
using System.Collections;

public abstract class BaseRandomEvent : MonoBehaviour 
{	
	[SerializeField]
	protected string mName = "Random Event";
	[SerializeField]
	protected float mLikelihood = 5.0f;

	public string EventName { get { return mName; } }
	public float Likelihood { get { return mLikelihood; } }

	public abstract void Execute();	
	
	public void Start()
	{
		Execute();
	}

}
