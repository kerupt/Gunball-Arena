using UnityEngine;
using System.Collections;

public class MeteorShower : BaseRandomEvent 
{
	[SerializeField]
	int mNumOfMeteors = 5;
	[SerializeField]
	float mTimeBetweenMeteors = 0.3f;
	[SerializeField]
	float mMaxMeteorOffset = 1.0f;
	[SerializeField]
	float mMaxMeteorAngle = 45.0f;
	[SerializeField]
	GameObject[] mMeteorPrefabs;

	[SerializeField]
	Transform mSpawnPosition;

	int mMeteorsLeft = 0;

	public override void Execute()
	{
		StartCoroutine(startShootingMeteors());
	}

	Vector3 getSpawnPosition()
	{
		Vector3 worldTopMiddle = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth/2, Camera.main.pixelHeight, 0.0f));
		worldTopMiddle = new Vector3(worldTopMiddle.x, worldTopMiddle.y - 1.0f, 0.0f);
		return worldTopMiddle;
	}

	IEnumerator startShootingMeteors()
	{
		mMeteorsLeft = mNumOfMeteors;
		for (; mMeteorsLeft > 0; mMeteorsLeft--)
		{
			instantiateMeteor();
			yield return new WaitForSeconds(mTimeBetweenMeteors);
		}
		Destroy (gameObject);
	}

	void instantiateMeteor()
	{
		Vector3 offset = new Vector3(Random.Range (-mMaxMeteorOffset, mMaxMeteorOffset), 0.0f, 0.0f);

		GameObject meteor = mMeteorPrefabs [(int)Random.Range (0, mMeteorPrefabs.Length)];

		Instantiate(meteor, getSpawnPosition() + offset, calculateMeteorAngle(meteor));
	}

	Quaternion calculateMeteorAngle(GameObject meteor)
	{
		return Quaternion.AngleAxis(meteor.transform.rotation.eulerAngles.z + Random.Range(-mMaxMeteorAngle, mMaxMeteorAngle), Vector3.forward);;
	}

	
}
