﻿using UnityEngine;
using System.Collections;

public class StunBothPlayersEvent : BaseRandomEvent 
{
	public override void Execute()
	{
		PlayerHealth player1 = GameManager.manager.Player1.GetComponent<PlayerHealth>();
		PlayerHealth player2 = GameManager.manager.Player2.GetComponent<PlayerHealth>();
		player1.TakeDamage(100.0f);
		player2.TakeDamage(100.0f);
	}
}
