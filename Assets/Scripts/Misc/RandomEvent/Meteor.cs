using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour 
{	
	[SerializeField]
	float mMoveSpeed;
	[SerializeField]
	float mLifeTime = 6.0f;

	void Start()
	{
		GetComponent<Rigidbody2D>().velocity = transform.up * mMoveSpeed;
		Destroy (gameObject,mLifeTime);
	}
}
