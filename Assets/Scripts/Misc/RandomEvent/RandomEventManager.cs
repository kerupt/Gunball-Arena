﻿using UnityEngine;
using System.Collections;

public class RandomEventManager : MonoBehaviour 
{
	[SerializeField]
	float mMinFrequency = 30.0f;
	[SerializeField]
	float mMaxFrequency = 45.0f;
	[SerializeField]
	BaseRandomEvent[] mRandomEventPrefabs;

	float mTotalWeight = 0;

	const string GENERATE_EVENTS_COROUTINE = "GenerateEvents";

	bool mActive = false;
	public bool ActivelyGeneratingEvents { get { return mActive; } private set { mActive = value;} }

	public AudioClip eventWarningSound;

	// Use this for initialization
	void Start () 
	{
		if (mRandomEventPrefabs.Length == 0)
		{
			Debug.Log ("No random events have been set");
		}
		else
		{
			calculateTotalWeight();
			StartGeneratingEvents();
		}
	}

	public void StartGeneratingEvents()
	{
		if (!ActivelyGeneratingEvents)
		{
			StartCoroutine("GenerateEvents");
			ActivelyGeneratingEvents = true;	
		}
	}

	public void StopGeneratingEvents()
	{
		if (ActivelyGeneratingEvents)
		{
			StopCoroutine("GenerateEvents");
			ActivelyGeneratingEvents = false;
		}
	}

	void calculateTotalWeight()
	{
		for (int i = 0; i < mRandomEventPrefabs.Length; i++)
		{
			mTotalWeight += mRandomEventPrefabs[i].Likelihood;
		}
	}

	IEnumerator GenerateEvents()
	{
		while (true) 
		{
			float randomTime = Random.Range(mMinFrequency, mMaxFrequency);
			yield return new WaitForSeconds (randomTime);
			Debug.Log ("Generating Event!");

			BaseRandomEvent randomEvent = selectRandomEvent();

			if (randomEvent != null)
			{
				if (HudManager.Instance != null)
				{
					HudManager.Instance.WarningText.DisplayWarning(randomEvent.EventName);
				}
				AudioSource.PlayClipAtPoint(eventWarningSound, transform.position);
				yield return new WaitForSeconds(0.5f);
				Instantiate(randomEvent);
			}
		}
	}

	void initiateRandomEvent()
	{
		//Show UI Warning

		BaseRandomEvent randomEvent = selectRandomEvent();

		if (randomEvent != null)
		{
			if (HudManager.Instance != null)
			{
				HudManager.Instance.WarningText.DisplayWarning(randomEvent.EventName);
			}
			
			Instantiate(randomEvent);
		}
	}

	BaseRandomEvent selectRandomEvent()
	{
		float randomNumber = Random.Range (0.0f, 1.0f) * mTotalWeight;

		float runningWeightTotal = 0.0f;
		for (int i = 0; i < mRandomEventPrefabs.Length; i++)
		{
			runningWeightTotal += mRandomEventPrefabs[i].Likelihood;
			if (randomNumber < runningWeightTotal)
			{
				return mRandomEventPrefabs[i];
			}
		}

		return null;
	}
}