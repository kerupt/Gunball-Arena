﻿using UnityEngine;
using System.Collections;

public class ExtraBallEvent : BaseRandomEvent 
{
	public override void Execute()
	{
		GameManager.manager.CurrentlyLoadedStage.SpawnBall(true);
	}
}
