﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FlashScreen : MonoBehaviour 
{

	private Image damageImage;                           
	public Color flashColour = new Color(1f, 0f, 0f, 0.3f); 

	void Start()
	{
		damageImage = transform.GetComponent<Image>();
	}

	public void Flash(int repeats = 0, float duration = 0.3f)
	{
		StartCoroutine(flashRepeatCoroutine(repeats,duration));
	}

	IEnumerator flashRepeatCoroutine(int repeats, float duration = 0.3f)
	{
		StartCoroutine(flashCoroutine(duration));
		if (repeats > 0)
		{
			for (int i = 0; i < repeats; i++)
			{
				yield return new WaitForSeconds(duration);
				StartCoroutine(flashCoroutine(duration));
			}
		}

	}

	IEnumerator flashCoroutine(float duration = 0.3f)
	{
		float progress = 0.0f;
		damageImage.color = flashColour;

		while (progress < 1.0f)
		{
			damageImage.color = Color.Lerp (flashColour, Color.clear, progress);
			progress += Time.deltaTime/duration;
			yield return new WaitForEndOfFrame();
		}
		damageImage.color = Color.clear;
	}

}
