﻿using UnityEngine;
using System.Collections;

public class DamagePlayerOnContactExplosion : DamagePlayerOnContact {
	protected override float CalculateDamage(GameObject go)
	{
		Vector2 dir = go.transform.position - transform.position;
		return (1 - dir.magnitude) * mDamage;
	}
}
