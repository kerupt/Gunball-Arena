﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WarningText : MonoBehaviour 
{
	[SerializeField]
	Text mWarningText;
	[SerializeField]
	float mFadeTime = 0.3f;
	[SerializeField]
	int mDisplayCount = 1;
	[SerializeField]
	AudioClip mWarningSound;

	bool displaying = false;

	public void DisplayWarning(string warningString)
	{
		if (mWarningSound != null)
		{
			AudioSource.PlayClipAtPoint(mWarningSound, transform.position);
		}

		mWarningText.text = warningString;
		StartCoroutine(fadeTextCoroutine());
	}

	IEnumerator fadeTextCoroutine()
	{
		if (!displaying)
		{
			displaying = true;
			Color originalColor = mWarningText.color;
			originalColor.a = 1.0f;
			Color transparentColor = originalColor;
			transparentColor.a = 0.0f;
			
			for (int iteration = 0; iteration < mDisplayCount; iteration++)
			{
				float progress = 0.0f;
				mWarningText.color = originalColor;

				while (progress < 1.0f)
				{
					mWarningText.color = Color.Lerp(originalColor, transparentColor, progress);
					progress += Time.deltaTime/mFadeTime;
					yield return new WaitForEndOfFrame();
				}

			}
			mWarningText.color = transparentColor;
			displaying = false;
		}


	}
}
